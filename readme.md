# Plateforme LMS pour CFA sanitaire / social

## Environnements de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* WAMP/MAMP/LAMP
* npm ou yarn

### Vérifier les pré-requis
```
$ symfony check:requirements
```
### Lancer environnement de développement

Une fois le projet cloné grâce à la commande 
```git clone ```
il vous suffit de passer à l'étape suivante. 

#### Créer un fichier environnement local

Une fois le projet cloné créer un fichier environnement local nommé
``` .env.local```
dans lequel vous allez remplacer les variables par défaut par les vôtre.

#### Installer la BDD et le mailCatcher grâce à docker

Docker est facultatif pour lancer le projet, cependant il permet d'installer une BDD et un serveur SMTP
en local automatiquement ce qui permet d'avoir une configuration homogene sur tous les environnements.

Pour fonctionner le fichier docker-compose.yaml a besoin de deux variables d'environnement
qu'il faudra définir dans votre fichier .env.local.

Pour lancer le fichier docker compose, il vous suffit d'utiliser les commandes : 
``` 
docker compose --env-file "chemin de votre fichier d'environnement"
```

#### Installer les dépendances

Pour faire fonctionner le projet il sera nécessaire d'installer les dépendances composer et npm.

Pour se faire un script prepare-dev est mis à votre disposition.

Pour lancer ce script il est normalement nécessaire d'avoir composer sur son pc.
Dans le but de garder la même version de composer sur tous les environnements, le projet est fourni
avec un fichier composer.phar qui permet d'executer composer sans l'installer.

**La seule dépendance nécessaire pour ce projet est PHP.**

Le script installera les dépendances du projet et il mettra en place la structure de la base de données.
Il peuplera également la base de données avec des données de factices.
Pour le lancer il suffit de taper la commande suivante :
```
php composer.phar run prepare-dev
```

#### Lancement du projet 

Pour lancer le projet il suffit de taper la commande :
```
php bin/console server:start
```

### Diagrammes de classe du projet

![AltText](./ReadMeImg/Diagramme%20UML.svg)
