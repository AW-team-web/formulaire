<?php

namespace App\DataFixtures;

use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ServiceFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $service1 = new Service();
        $service1->setName("Code IT")
            ->setLevel("Niveau BAC à BAC+3")
            ->setDiplomas([
                "Technicien(ne) d’Assistance en Informatique - TAI",
                "Formation Développeur/ développeuse e-commerce",
                "Technicien(ne) Supérieur(e) Systèmes et Réseaux - TSSR",
                "Développeur/ développeuse Web/Web Mobile - DWWM",
                "Certification Microsoft Technology Associate (MTA)",
                "Administrateur(trice) d'Infrastructures Sécurisées - AIS",
                "Concepteur(trice) Développeur(euse) d'Application - CDA",
                "Administrateur/ Administratrice Système DevOps"
            ]);
        $service2 = new Service();
        $service2->setName("Médico-social")
            ->setLevel("Niveau BAC+2 à BAC+3")
            ->setDiplomas([
                "Diplôme d'État Aide Soignant(e) - DEAS",
                "Diplôme d'État Infirmier(e) - DEI"
            ]);
        $service3 = new Service();
        $service3->setName("Petite enfance")
            ->setLevel("Niveau CAP et Concours")
            ->setDiplomas([
                "Accompagnant(e) Éducatif Petite Enfance - AEPE",
                "Agent territorial spécialisé des écoles maternelles - ATSEM"
            ]);
        $service4 = new Service();
        $service4->setName("Services à la personne")
            ->setLevel("Niveau CAP à Bac+2")
            ->setDiplomas([
                "Assistant(e) Technique en Milieu Familial et Collectif - ATMFC",
                "Agent de Propreté et d’Hygiène - APH",
                "Accompagnement, Soins et Services à la Personne - ASSP",
                "Agent de Service Médico-Social - ASMS",
                "Assistant(e) De Vie aux Familles - ADVF",
                "Responsable de Secteur Services à la Personne - RSSP"
            ]);
        $manager->persist($service1);
        $manager->persist($service2);
        $manager->persist($service3);
        $manager->persist($service4);
        $manager->flush();
    }
}
