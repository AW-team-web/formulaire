<?php

namespace App\DataFixtures;

use App\Entity\Adress;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher){
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $this->formateurFixture($manager, $this->hasher);
        $this->candidatFixture($manager, $this->hasher);
        $this->adminFixture($manager, $this->hasher);
        $manager->flush();
    }

    private function candidatFixture(ObjectManager $manager, UserPasswordHasherInterface $hasher): void
    {
        $candidat = new User();
        $candidatAdress = new Adress();
        $candidat->setEmail("candidat@apprenti-webeur.fr");
        $candidat->setFirstname("John");
        $candidat->setLastname("Doe");
        $candidat->setPhone(0671456725);
        $candidat->setRoles(["ROLE_CANDIDAT"]);
        $candidatAdress->setAddress("12 impasse du test");
        $candidatAdress->setZipcode(12000);
        $candidatAdress->setCountry("France");
        $candidatAdress->setCity("TestVille");
        $candidat->setAdress($candidatAdress);
        $candidat->setPassword($hasher->hashPassword($candidat, "candidat"));
        $manager->persist($candidat);
    }

    private function formateurFixture(ObjectManager $manager, UserPasswordHasherInterface $hasher): void
    {
        $formateur = new User();
        $formateurAdress = new Adress();
        $formateur->setEmail("formateur@apprenti-webeur.fr");
        $formateur->setFirstname("Jane");
        $formateur->setLastname("Doe");
        $formateur->setPhone(0671456727);
        $formateur->setRoles(["ROLE_FORMATEUR"]);
        $formateurAdress->setAddress("13 impasse du test");
        $formateurAdress->setZipcode(12000);
        $formateurAdress->setCountry("France");
        $formateurAdress->setCity("TestVille");
        $formateur->setAdress($formateurAdress);
        $formateur->setPassword($hasher->hashPassword($formateur, "formateur"));
        $manager->persist($formateur);
    }

    private function adminFixture(ObjectManager $manager, UserPasswordHasherInterface $hasher): void
    {
        $admin = new User();
        $adminAdress = new Adress();
        $admin->setEmail("admin@apprenti-webeur.fr");
        $admin->setFirstname("Jean-françois");
        $admin->setLastname("Admin");
        $admin->setPhone(0671456727);
        $admin->setRoles(["ROLE_ADMIN"]);
        $adminAdress->setAddress("14 impasse du test");
        $adminAdress->setZipcode(12000);
        $adminAdress->setCountry("France");
        $adminAdress->setCity("TestVille");
        $admin->setAdress($adminAdress);
        $admin->setPassword($hasher->hashPassword($admin, "admin"));
        $manager->persist($admin);
    }
}