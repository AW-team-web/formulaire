<?php

namespace App\DataFixtures;

use App\Entity\Adress;
use App\Entity\Course;
use App\Entity\CourseModule;
use App\Entity\PaymentType;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class CourseFixtures extends Fixture
{
    private SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager): void
    {
        $datas = [
            [
                'name' => 'Technicien Supérieur Systèmes et Réseaux',
                'menu_name' => 'Tssr',
                'formation_short_description' => 'Vous aimez dépannez vos amis dans l\'informatique et vous appréciez le réseau ? Alors cette formation est faite pour vous !',
                'formation_long_description' => 'Le/la technicien(ne) supérieur(e) systèmes et réseaux est un(e) informaticien(ne) qui participe à l’installation, la configuration, la sécurisation et la maintenance des équipements informatiques et d’infrastructure IT d’une entreprise. Il/elle assure également la relation avec les usagers lors d’incidents techniques ou de prise de poste.
                    Le profil du/de la technicien(ne) systèmes et réseaux est celui d\'un(e) technicien(ne) capable de comprendre les besoins en équipement, en configuration et maintenance des équipements IT. Il/elle suit des standards de gestion de services informatiques comme ITIL et des standards de sécurisation des équipements, notamment en appliquant les préconisations de l’ANSSI.',
                'course_public' => 'Accessible à tous',
                'marketing_prerequisites' => 'Aucun pré-requis',
                'professor' => 'Nicolas Sahuc*',
                'student_number' => 12,
                'duration' => 36,
                'duration_unit' => 'semaines',
                'end_course_level' => 'Bac+2',
                'format' => 'Présentiel ou distanciel',
                'title' => 'RNCP de niveau 5 (équivalent Bac + 2)',
                'availability' => 'Toute l\'année',
                'in_progress_skills' => [
                    'Assistance aux utilisateurs en centre de services',
                    'Maintenance, exploitation et sécurisation d’une infrastructure centralisée',
                    'Maintenance, Exploitation et sécurisation d’une infrastructure distribuée'
                ],
                'end_skills' => [
                    'Assister les utilisateurs en centre de services',
                    'Maintenir, exploiter et sécuriser une infrastructure centralisée',
                    'Maintenir et exploiter une infrastructure distribuée et contribuer à sa sécurisation'
                ],
                'activities' => [
                    'Mettre en service un équipement numérique',
                    'Assister les utilisateurs sur leurs équipements numériques',
                    'Gérer les incidents et les problèmes',
                    'Assister à l\'utilisation des ressources collaboratives',
                    'Maintenir et exploiter le réseau local et la téléphonie',
                    'Sécuriser les accès à Internet',
                    'Maintenir et exploiter un environnement virtualisé',
                    'Maintenir et exploiter un domaine Active Directory et les serveurs Windows',
                    'Maintenir et exploiter un serveur Linux',
                    'Configurer les services de déploiement et de terminaux clients légers',
                    'Automatiser les tâches à l\'aide de scripts',
                    'Maintenir et sécuriser les accès réseaux distants',
                    'Superviser l\'infrastructure',
                    'Intervenir dans un environnement de Cloud Computing',
                    'Assurer sa veille technologique'
                ],
                'goals' => [
                    'Le titre RNCP de Technicien Supérieur Systèmes et Réseaux (Niveau 5 ) en partenariat avec la région Occitanie.'
                ],
                'jobs' => [
                    'Technicien(ne) supérieur(e) systèmes et réseaux',
                    'Technicien(ne) support',
                    'Technicien(ne) réseau'
                ],
                'pricing' => [
                    [
                        'payment_type' => 'Auto-financement',
                        'payment_subtitle' => 'A partir de',
                        'payment_amount' => 1500,
                        'payment_unit' => '€',
                        'payment_description' => 'Vérifiez vos aides afin de trouver celle qui vous correspond, et ainsi ne rien à avoir à débourser.'
                    ],
                    [
                        'payment_type' => 'Pole emploi/organisme',
                        'payment_subtitle' => 'Prise en charge à',
                        'payment_amount' => 100,
                        'payment_unit' => '%',
                        'payment_description' => 'Profitez de l\'alternance qui subventionne à une hauteur de 100% votre formation.'
                    ],
                    [
                        'payment_type' => 'Paiement en 4 fois',
                        'payment_subtitle' => 'A partir de',
                        'payment_amount' => 375,
                        'payment_unit' => '€/mois',
                        'payment_description' => 'Payez votre formation en 4 fois sans frais.'
                    ]
                ],
                'formation_program' => [
                    [
                        'program_item_name' => 'Bien démarrer ma formation',
                        'items' => [
                            'Guide de formation',
                            'Les outils du parcours TSSR',
                            'Le TSSR dans le monde d’aujourd’hui'
                        ]
                    ],
                    [
                        'program_item_name' => 'Apprendre à Apprendre',
                        'items' => [
                            'Les clés de l’apprentissage',
                            'La méthode Apprenti Webeur',
                            'Les phénomènes liés au temps',
                            'L’art de mieux savoir gérer son temps',
                            'Le stress : fléau du XXIe siècle ?',
                            'Les techniques pour maitriser son stress'
                        ]
                    ],
                    [
                        'program_item_name' => 'Favoriser son immersion professionnelle et son employabilité',
                        'items' => [
                            'La réalisation d’une enquête métier',
                            'Un comportement professionnel',
                            'Le développement des synergies',
                            'La lettre de motivation',
                            'Construire son CV',
                            'L’entretien d’embauche',
                            'Travailler son identité digitale',
                            'Pitch anything – Les clés'
                        ]
                    ],
                    [
                        'program_item_name' =>  'Découvrir des témoignages',
                        'items' => [
                            'Le CTO de Nutrition et Santé',
                            'Témoignage d’un ancien apprenant',
                            'Découvrez une agence de recrutement pour les TSSR'
                        ]
                    ],
                    [
                        'program_item_name' =>  'Réussir mon titre RNCP',
                        'items' => [
                            'Les évaluations : Simple et ECF',
                            'La Période d’Application en Entreprise',
                            'La certification plateaux'
                        ]
                    ],
                    [
                        'program_item_name' =>  'Assister les utilisateurs en centre de services',
                        'items' => [
                            'Installer, configurer et déployer un poste sous Windows',
                            'Installer, configurer et déployer un poste sous Linux',
                            'Installer, configurer et déployer un poste sous Mac OS',
                            'Utiliser un logiciel de virtualisation',
                            'Gérer le parc d’équipements',
                            'Installer et paramétrer un appareil',
                            'Paramétrer la téléphonie IP',
                            'Sécuriser les équipements numériques',
                            'Câbler les réseaux',
                            'Paramétrer les adresses IP',
                            'Se rappeler de la notion : les droits linux',
                            'Configurer différents systèmes',
                            'Utiliser les fonctions avancées',
                            'Configurer la messagerie sur Smartphone et tablette Android',
                            'Assister les utilisateurs par téléphone',
                            'Rédiger les procédures d’installation',
                            'Respecter et faire respecter les bonnes pratiques élémentaires de sécurité',
                            'Utiliser de manière éco-responsable les équipements numériques',
                            'Gérer les situations de pannes/dépannage',
                            'Appliquer les bonnes pratiques en centre de services',
                            'Stocker en ligne',
                            'Les outils collaboratifs',
                        ]
                    ],
                    [
                        'program_item_name' =>  'Maintenir, exploiter et sécuriser une infrastructure centralisée',
                        'items' => [
                            'Identifier le rôle des couches des modèles osi et tcp-ip',
                            'Implémenter un plan d’adressage ip',
                            'Intervenir sur un autocommutateur téléphonique IP',
                            'Configurer la connectivité filaire et sans fil',
                            'Paramétrer et sécuriser un routeur',
                            'Configurer les vlans',
                            'Connaitre les notions de sécurité',
                            'Mettre en œuvre un équipement de sécurité',
                            'Filtrer et journaliser les flux d’un réseau',
                            'Connaitre l’environnement d’un datacenter',
                            'Installer et configurer un serveur windows autonome',
                            'Installer les rôles ActiveDirectory et dns',
                            'Gérer le domaine',
                            'Ajouter un serveur de fichiers dans le domaine',
                            'Installer le rôle serveur dhcp',
                            'Configurer et dépanner les stratégies de groupe : GPO',
                            'Active directory',
                            'Installer et paramétrer un hyperviseur',
                            'Intervenir sur une infrastructure centralisée d’hyperviseurs',
                            'Gérer les sauvegardes de l’environnement virtualisé',
                            'Mettre en œuvre la tolérance de pannes et d’équilibrage de charge',
                            'Installer et configurer un serveur debian',
                            'Installer une application lamp',
                            'Renforcer la sécurité du système'
                        ]
                    ],
                    [
                        'program_item_name' =>  'Maintenir et exploiter une infrastructure distribuée et contribuer à sa sécurisation',
                        'items' => [
                            'Installer et paramétrer un service de déploiement d’images',
                            'Installer et configurer un serveur wsus',
                            'Installer et configurer un serveur client léger',
                            'Automatiser les tâches à l’aide de scripts sous windows',
                            'Automatiser les tâches à l’aide de scripts sous linux',
                            'Mettre en œuvre un protocole de routage',
                            'Intervenir sur une infrastructure de clés publiques',
                            'Installer le serveur OpenSSH',
                            'Installer Open SSL',
                            'Installer un VPN',
                            'Installer OpenVPN et Wireguard',
                            'Wireguard',
                            'Installer Nagios ou Centreon',
                            'Exploiter les journaux des équipements',
                            'Analyser les flux réseaux',
                            'Utiliser un outil de gestion centralisée des périphériques',
                            'Connaitre les environnements de cloud computing',
                            'Administrer la messagerie en cloud',
                            'Assister les utilisateurs sur les problématiques d’hébergement',
                            'Assurer sa veille technologique'
                        ]
                    ],
                    [
                        'program_item_name' => 'Période d’Application en entreprise',
                        'items' => [
                            'Avant la PAE',
                            'Pendant la PAE',
                            'Après la PAE',
                        ]
                    ]
                ],
                'prerequisites' => [
                    'Aucun pré-requis technique et de diplômes',
                    'Très forte motivation pour s’engager dans une formation intense',
                    'Bonne expression à l’oral et à l’écrit'
                ]
            ],
            [
                'name' => 'Wordpress',
                'menu_name' => 'Wordpress',
                'formation_short_description' => 'Cette formation WordPress vous permettra de réaliser vous même, votre Site internet, votre Blog ou votre E-commerce et ce, de manière totalement personnalisée et professionnelle.',
                'formation_long_description' => 'Apprenti Webeur vous propose une vraie formation WordPress au format présentiel, avec un formateur expérimenté qui répondra aux questions que vous vous posez.10 jours de formation assorties de supports pédagogiques, d’exercices pratiques et d’un suivi post formation pour apprendre et pour comprendre !',
                'course_public' => 'Accessible à tous',
                'marketing_prerequisites' => 'Aucun pré-requis',
                'professor' => 'Nicolas Sahuc*',
                'student_number' => 12,
                'duration' => 1,5,
                'duration_unit' => 'semaines',
                'end_course_level' => 'Certificat',
                'format' => 'Présentiel ou distanciel',
                'title' => 'Certificat',
                'availability' => 'Toute l\'année',
                'in_progress_skills' => [
                    'Introduction à l\'univers Wordpress',
                    'Optimiser et personnaliser son site web',
                    'Mise en place d\'un site e-commerce (module de paiement,gestion commande...)',
                    'Sécuriser son site et ses utilisateurs'
                ],
                'end_skills' => [],
                'activities' => [],
                'goals' => [
                    'Un certificat de formation attestant de votre réussite.'
                ],
                'jobs' => [
                    'Développeur Wordpress',
                    'Webmaster Wordpress'
                ],
                'pricing' => [
                    [
                        'payment_type' => 'Auto-financement',
                        'payment_subtitle' => 'A partir de',
                        'payment_amount' => 1500,
                        'payment_unit' => '€',
                        'payment_description' => 'Vérifiez vos aides afin de trouver celle qui vous correspond, et ainsi ne rien à avoir à débourser.'
                    ],
                    [
                        'payment_type' => 'Pole emploi/organisme',
                        'payment_subtitle' => 'Prise en charge à',
                        'payment_amount' => 100,
                        'payment_unit' => '%',
                        'payment_description' => 'Profitez de l\'alternance qui subventionne à une hauteur de 100% votre formation.'
                    ],
                    [
                        'payment_type' => 'Paiement en 4 fois',
                        'payment_subtitle' => 'A partir de',
                        'payment_amount' => 375,
                        'payment_unit' => '€/mois',
                        'payment_description' => 'Payez votre formation en 4 fois sans frais.'
                    ]
                ],
                'formation_program' => [
                    [
                        'program_item_name' => 'Les cas d\'utilisation de Wordpress
                        ',
                        'items' => array()
                    ],
                    [
                        'program_item_name' => 'Introduction aux systèmes de gestion de contenu CMS
                        ',
                        'items' => array()
                    ],
                    [
                        'program_item_name' => 'Installer Worpress en local
                        ',
                        'items' => array()
                    ],
                    [
                        'program_item_name' =>  'Comprendre l\’interface d’administration',
                        'items' => array()
                    ],
                    [
                        'program_item_name' =>  'Sélectionner, configurer et utiliser un thème',
                        'items' => array()
                    ],
                    [
                        'program_item_name' =>  'Personnaliser un thème',
                        'items' => array()
                    ],
                    [
                        'program_item_name' =>  'Sécuriser son site WordPress',
                        'items' => array()
                    ],
                    [
                        'program_item_name' =>  'Optimiser son référencement SEO',
                        'items' => array()
                    ],
                    [
                        'program_item_name' => 'Déployer son site en ligne',
                        'items' => array()
                    ]
                ],
                'prerequisites' => [
                    'Aucun pré-requis technique et de diplômes',
                    'Très forte motivation pour le numérique'
                ]
            ],
            [
                'name' => 'Web entrepreneur',
                'menu_name' => 'Web entrepreneur',
                'formation_short_description' => 'Monter son business sur internet pour devenir web entrepreneur ne demande pas un gros investissement de départ.Apprenez à lancer votre site e-commerce, le développer, et créez une entreprise rentable.',
                'formation_long_description' => 'Permettre à toute personne désirant créer son activité de e-commerce d’avoir une approche concrète et de maitriser l’ensemble des étapes.
                    La formation et les conseils prodigués concourront à la sécurisation et à la pèrennisation de l’activité.
                    Cette certification constituera un atout important pour rassurer et convaincre les investisseurs et/ou financeurs du projet.
                    Elle s’inscrit également dans le parcours d’un demandeur d’emploi ouvrant à une première expérience et à une polyvalence dans le domaine du e-commerce.
                    Enfin, elle peut s’adresser à toute personne exerçant une activité commerciale traditionnelle pour étendre son activité au e-commerce.
                    Ce parcours permet une découverte des dernières innovations marketing utilisées sur le web-marchand.',
                'course_public' => 'Accessible à tous',
                'marketing_prerequisites' => 'Aucun pré-requis',
                'professor' => 'Nicolas Sahuc*',
                'student_number' => 12,
                'duration' => 4,
                'duration_unit' => 'semaines',
                'end_course_level' => 'Certificat',
                'format' => 'Présentiel ou distanciel',
                'title' => 'Certificat',
                'availability' => 'Toute l\'année',
                'in_progress_skills' => [
                    'Construire et maitriser son projet en mettant en avant ses atouts',
                    'Analyser, évaluer le marché et identifier les cibles potentielles',
                    'Concevoir un plan marketing et définir son seuil de rentabilité',
                    'Concevoir, administrer et sécuriser un site de e-commerce',
                    'Présenter et mettre en avant ses produits en utilisant des outils innovants en webmarketing',
                    'Communiquer sur les réseaux sociaux et connaitre les leviers pour cibler sa clientèle',
                    'Maitriser la relation client de la création du devis au service aprés-vente',
                    'Présenter son projet et convaincre un financeur/investisseur'
                ],
                'end_skills' => [],
                'activities' => [],
                'goals' => [
                    'Un certificat de formation attestant de votre réussite.'
                ],
                'jobs' => [
                    'Auto Entrepreneur',
                    'Assistant en transition numérique',
                    'Gestionnaire de site E-Commerce'
                ],
                'pricing' => [
                    [
                        'payment_type' => 'Auto-financement',
                        'payment_subtitle' => 'A partir de',
                        'payment_amount' => 1500,
                        'payment_unit' => '€',
                        'payment_description' => 'Vérifiez vos aides afin de trouver celle qui vous correspond, et ainsi ne rien à avoir à débourser.'
                    ],
                    [
                        'payment_type' => 'Pole emploi/organisme',
                        'payment_subtitle' => 'Prise en charge à',
                        'payment_amount' => 100,
                        'payment_unit' => '%',
                        'payment_description' => 'Profitez de l\'alternance qui subventionne à une hauteur de 100% votre formation.'
                    ],
                    [
                        'payment_type' => 'Paiement en 4 fois',
                        'payment_subtitle' => 'A partir de',
                        'payment_amount' => 375,
                        'payment_unit' => '€/mois',
                        'payment_description' => 'Payez votre formation en 4 fois sans frais.'
                    ]
                ],
                'formation_program' => [
                    [
                        'program_item_name' => 'Microentreprise, le kit de démarrage',
                        'items' => array()
                    ],
                    [
                        'program_item_name' => 'Stratégie produit',
                        'items' => array()
                    ],
                    [
                        'program_item_name' => 'Créer son premier site de e-commerce',
                        'items' => array()
                    ],
                    [
                        'program_item_name' =>  'Stratégie marketing',
                        'items' => array()
                    ]
                ],
                'prerequisites' => [
                    'Aucun pré-requis technique et de diplômes',
                    'Très forte motivation pour le numérique'
                ]
            ],
            [
                'name' => 'Agilité',
                'menu_name' => 'Agilité',
                'formation_short_description' => 'Découvrez notre formation aux méthodes agiles de gestion et amorçage de projet.',
                'formation_long_description' => 'Découvrir les fondamentaux des méthodes de gestion de projet agile, et de méthodes d’amorçage de projet (validation de concept, approche prototypale, etc.) qui y sont associées (Scrum, Kanban et Lean startup principalement), avec un focus sur Scrum, afin de pouvoir le mettre en œuvre dans le cadre d’un projet réalisé en équipe.',
                'course_public' => 'Accessible à tous',
                'marketing_prerequisites' => 'Aucun pré-requis',
                'professor' => 'Rémy SAHAGUIAN*',
                'student_number' => 12,
                'duration' => 4,
                'duration_unit' => 'jours',
                'end_course_level' => 'Certificat',
                'format' => 'Présentiel ou distanciel',
                'title' => 'Certificat',
                'availability' => 'Toute l\'année',
                'in_progress_skills' => [
                    'Planifier et organiser le travail d’une équipe en mode agile',
                    'Gérer un projet de manière efficiente et itérative',
                    'Pratiquer le méthodologie Scrum',
                    'Pratiquer le Kanban',
                    'Appliquer des formats de facilitation dans la conduite de projets et de réunions',
                    'Organiser et mettre en œuvre l’amorçage et le lancement de projets sur un mode lean startup',
                    'Utiliser plusieurs méthodes relevant de l’agilité',
                    'S’organiser, individuellement ou en groupe',
                    'Communiquer clairement avec des collaborateurs dans le cadre d’un projet',
                    'S’adapter au changement et être flexible sur un projet soumis à changements fréquents',
                    'Coordonner et manager des collaborateurs'
                ],
                'end_skills' => [],
                'activities' => [],
                'goals' => [
                    'Un certificat de formation attestant de votre réussite.'
                ],
                'jobs' => [
                    'Chef de projet',
                    'Assistant en transition numérique',
                    'Manager'
                ],
                'pricing' => [
                    [
                        'payment_type' => 'Auto-financement',
                        'payment_subtitle' => 'A partir de',
                        'payment_amount' => 1500,
                        'payment_unit' => '€',
                        'payment_description' => 'Vérifiez vos aides afin de trouver celle qui vous correspond, et ainsi ne rien à avoir à débourser.'
                    ],
                    [
                        'payment_type' => 'Pole emploi/organisme',
                        'payment_subtitle' => 'Prise en charge à',
                        'payment_amount' => 100,
                        'payment_unit' => '%',
                        'payment_description' => 'Profitez de l\'alternance qui subventionne à une hauteur de 100% votre formation.'
                    ],
                    [
                        'payment_type' => 'Paiement en 4 fois',
                        'payment_subtitle' => 'A partir de',
                        'payment_amount' => 375,
                        'payment_unit' => '€/mois',
                        'payment_description' => 'Payez votre formation en 4 fois sans frais.'
                    ]
                ],
                'formation_program' => [
                    [
                        'program_item_name' => 'S’adapter et faire converger vers le client',
                        'items' => array()
                    ],
                    [
                        'program_item_name' => 'Activer l’émergence et une prise en compte réaliste et flexible',
                        'items' => array()
                    ],
                    [
                        'program_item_name' => 'Réduire les écarts entre « Cycle d’évolution d’un produit/service » et « Processus d’une organisation »',
                        'items' => array()
                    ]
                ],
                'prerequisites' => [
                    'Aucun pré-requis technique et de diplômes',
                    'Très forte motivation pour le numérique'
                ]
            ]
        ];

        foreach ($datas as $data) {
            $course = new Course();
            $course
                ->setName($data['name'])
                ->setMenuName($data['menu_name'])
                ->setSlug($this->slugger->slug($data['name']))
                ->setCourseShortDescription($data['formation_short_description'])
                ->setCourseLongDescription($data['formation_long_description'])
                ->setCoursePublic($data['course_public'])
                ->setCourseMarketingPrerequisite($data['marketing_prerequisites'])
                ->setCourseProfessor($data['professor'])
                ->setCourseStudentNumber($data['student_number'])
                ->setCourseDuration($data['duration'])
                ->setCourseDurationUnit($data['duration_unit'])
                ->setCourseEndLevel($data['end_course_level'])
                ->setCourseFormat($data['format'])
                ->setCourseEndTitle($data['title'])
                ->setCourseAvailability($data['availability'])
                ->setCourseInProgressSkills($data['in_progress_skills'])
                ->setCourseEndGoals($data['goals'])
                ->setCourseEndSkills($data['end_skills'])
                ->setCourseActivities($data['activities'])
                ->setCourseEndJobs($data['jobs'])
                ->setCoursePrerequisites($data['prerequisites'])
                ->setPageCreated(true);
            foreach ($data['formation_program'] as $module_data){
                $module = new CourseModule();
                $module->setModuleItems($module_data['items']);
                $module->setModuleName($module_data['program_item_name']);
                $course->addCourseModule($module);
            }
            foreach ($data['pricing'] as $payment){
                $paymentType = new PaymentType();
                $paymentType->setType($payment['payment_type'])
                    ->setSubtitle($payment['payment_subtitle'])
                    ->setAmount($payment['payment_amount'])
                    ->setUnit($payment['payment_unit'])
                    ->setDescription($payment['payment_description']);
                $course->addCoursePricing($paymentType);
            }

            $manager->persist($course);
        }

        $manager->flush();
    }
}
