<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends \Doctrine\Bundle\FixturesBundle\Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $code_it = new Category();
        $code_it->setName("Code It");
        $code_it->setImgUrl("codeIt.png");
        $manager->persist($code_it);
        $design = new Category();
        $design->setName("Design");
        $design->setImgUrl("design.png");
        $manager->persist($design);
        $gestion_de_projet = new Category();
        $gestion_de_projet->setName("Gestion de projet");
        $gestion_de_projet->setImgUrl("agile.png");
        $manager->persist($gestion_de_projet);
        $medico_social = new Category();
        $medico_social->setName("Medico-social");
        $medico_social->setImgUrl("medico.png");
        $manager->persist($medico_social);
        $petite_enfance = new Category();
        $petite_enfance->setName("Petite enfance");
        $petite_enfance->setImgUrl("enfance.png");
        $manager->persist($petite_enfance);
        $person_service = new Category();
        $person_service->setName("Service à la personne");
        $person_service->setImgUrl("service.png");
        $manager->persist($person_service);
        $manager->flush();
    }
}