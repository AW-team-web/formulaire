<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Services\MailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route(path="/contact", name="contact")
     */
    public function contactForm(Request $request, MailService $mailer): Response
    {
        $form = $this->createForm(ContactType::class);
        $form -> handleRequest($request);
        if($form -> isSubmitted() && $form -> isValid()){
            if ($mailer->sendContactMail($form->getData())){
                $this->addFlash("success", "Le mail a été envoyé");
                return $this->redirectToRoute("home");
            } else {
                $this->addFlash("danger","Le mail ne s'est pas envoyé correctement veuillez contacter un administrateur");
                return $this->redirectToRoute("contact");
            }
        }
        return $this->render('contact/index.html.twig', [
            "form" => $form->createView()
        ]);
    }

}
