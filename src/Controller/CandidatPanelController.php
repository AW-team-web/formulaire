<?php

namespace App\Controller;

use App\Form\ProfileUpdateFormType;
use App\Repository\NominationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CandidatPanelController extends AbstractController
{
    /**
     * @Route (name="candidat_profile", path="/candidat/profile/nominations")
     */
    public function index(NominationRepository $repository): Response
    {
        $user = $this->getUser();
        $nomination = [];
        if (!is_null($user)){
            $nomination = $repository->findBy(["user" => $user]);
        }
        return $this->render('candidat_panel/index.html.twig', [
            "nominations"=>$nomination
        ]);
    }

    /**
     * @Route (name="candidat_profile_update", path="/candidat/profile/update")
     */
    public function updateProfile(EntityManagerInterface $em, Request $request){
        $user = $this->getUser();
        $form = $this->createForm(ProfileUpdateFormType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            try {
                $em->flush();
                $this->addFlash("success", "Votre profil à été mis à jour.");
                return $this->redirectToRoute("candidat_profile_update");
            } catch (\Exception $e){
                $this->addFlash("danger", "Un problème est survenue veuillez contacter un administrateur");
                return $this->redirectToRoute("candidat_profile_update");
            }
        }
        return $this->render('candidat_panel/update_profile.html.twig', [
           "form" => $form->createView()
        ]);
    }
}
