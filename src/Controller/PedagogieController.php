<?php

namespace App\Controller;

use App\Repository\ServiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PedagogieController extends AbstractController
{
    /**
     * @Route("/pedagogie", name="app_pedagogie")
     */
    public function index(ServiceRepository $repo): Response
    {
        $services = $repo->findAll();
        return $this->render('pedagogie/index.html.twig', [
            'services' => $services,
        ]);
    }
}
