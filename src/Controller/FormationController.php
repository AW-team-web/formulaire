<?php

namespace App\Controller;

use App\Repository\CourseRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FormationController extends AbstractController
{
    /**
     * @Route(path="/formation/{id}", name="formation_item")
     */
    public function formationSingle(int $id, CourseRepository $repo): Response
    {
        $course = $repo->findByCourseLinked($id);
        if ($course === null){
            throw $this->createNotFoundException("Ce cours n'existe pas");
        }
        return $this->render('formation/index.html.twig', [
            "course" => $course
        ]);
    }

      /**
     * @Route(path="/formationList", name="formationList")
     */
    public function formationList(CategoryRepository $catrepo) : Response
    {
        $categories = $catrepo->findAll();
        return $this->render('formation/_formation_list.html.twig', [
            "categories" => $categories
        ]);
    }




}
