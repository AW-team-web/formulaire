<?php

namespace App\Controller;

use App\Entity\AwData;
use App\Entity\Diploma;
use App\Entity\GoalsInfo;
use App\Entity\Job;
use App\Entity\Lang;
use App\Entity\Nomination;
use App\Entity\PersonnalInfos;
use App\Entity\Situation;
use App\Entity\Skill;
use App\Entity\User;
use App\Form\AwDataFormType;
use App\Form\DiplomaCollectionType;
use App\Form\DiplomaFormType;
use App\Form\GoalsInfoFormType;
use App\Form\JobFormType;
use App\Form\JobsCollectionFormType;
use App\Form\LangFormType;
use App\Form\LangsCollectionFormType;
use App\Form\PersonnalInfoFormType;
use App\Form\SituationFormType;
use App\Form\SkillFormType;
use App\Form\SkillsCollectionFormType;
use App\Repository\CourseRepository;
use App\Repository\NominationRepository;
use App\Services\MailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NominationController extends AbstractController
{
    /**
     * @Route(path="/nomination/{id}/1", name="nomination_personnal_info")
     */
    public function nominationPersonnalInfos(Request $request, int $id, CourseRepository $crepo, NominationRepository $nrepo): Response
    {

        if ($nrepo->getWaitingNominationNumber($this->getUser()) >= 1){
            $this->addFlash("warning", "Veuillez attendre que votre candidature soit refusée ou validée pour en soumettre une nouvelle.");
            return $this->redirectToRoute("home");
        }
        if (!$this->isGranted("IS_AUTHENTICATED_FULLY")){
            $this->addFlash("warning", "Vous devez être connecté pour déposer votre candidature. Veuillez vous connecter ou créer un compte.");
            return $this->redirectToRoute("login");
        }
        $personnalInfos = new PersonnalInfos();
        $form = $this->createForm(PersonnalInfoFormType::class, $personnalInfos);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $session = $request->getSession();
            $session->set("nomination", [
                "nominationName" => $crepo->findOneById($id)->getName(),
                "personnalInfos" => $personnalInfos
            ]);
            return $this->redirectToRoute("nomination_situation", ["id" => $id]);
        }
        return $this->render('nomination/nominationPersonnalInfos.html.twig', [
            "form" => $form->createView(),
            'stepNumber' => 1
        ]);
    }

    
    /**
     * @Route(path="/nomination/{id}/2", name="nomination_situation")
     */
    public function nominationSituation(Request $request, int $id): Response
    {
        $session= $request->getSession();
        $nominationArray= $session->get("nomination", []);
        if(!key_exists("personnalInfos", $nominationArray)){
            $this->addFlash("warning", "Veuillez suivre le processus de candidature et remplir les étapes une par une");
            return $this->redirectToRoute("nomination_personnal_info", ["id" => $id]);
        }
        $situation = new Situation();
        $form = $this->createForm(SituationFormType::class, $situation);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $nominationArray["situation"] = $situation;
            $session->set("nomination", $nominationArray);
            return $this->redirectToRoute("nomination_diploma", ["id" => $id]);
        }
        return $this->render('nomination/nominationSituation.html.twig', [
            "form" => $form->createView(),
            'stepNumber' => 1
        ]);
    }
    /**
     * @Route(path="/nomination/{id}/3", name="nomination_diploma")
     */
    public function nominationDiplomas(Request $request, int $id): Response
    {
        $session= $request->getSession();
        $nominationArray= $session->get("nomination", []);
        if(!key_exists("situation", $nominationArray)){
            $this->addFlash("warning", "Veuillez suivre le processus de candidature et remplir les étapes une par une");
            return $this->redirectToRoute("nomination_personnal_info", ["id" => $id]);
        }
        if (!isset($nominationArray["diplomas"])){
            $nominationArray["diplomas"] = [];
            $session->set("nomination", $nominationArray);
        }
        $form= $this->createForm(DiplomaCollectionType::class, $nominationArray);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $nominationArray["diplomas"] = $form->get("diplomas")->getData();
            $session->set("nomination", $nominationArray);
            return $this->redirectToRoute("nomination_skills", ["id" => $id]);
        }
        return $this->render('nomination/nominationDiplomas.html.twig', [
            "form" => $form->createView(),
            "id" => $id,
            'stepNumber' => 2
        ]);
    }

    /**
     * @Route(path="/nomination/{id}/4", name="nomination_skills")
     */
    public function nominationSkills(Request $request, int $id): Response
    {
        $session= $request->getSession();
        $nominationArray= $session->get("nomination", []);
        if(!key_exists("diplomas", $nominationArray)){
            $this->addFlash("warning", "Veuillez suivre le processus de candidature et remplir les étapes une par une");
            return $this->redirectToRoute("nomination_personnal_info", ["id" => $id]);
        }
        if (!isset($nominationArray["skills"])){
            $nominationArray["skills"] = [];
            $session->set("nomination", $nominationArray);
        }
        $form= $this->createForm(SkillsCollectionFormType::class, $nominationArray);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $nominationArray["skills"] = $form->get("skills")->getData();
            $session->set("nomination", $nominationArray);
            return $this->redirectToRoute("nomination_lang", ["id" => $id]);
        }
        return $this->render('nomination/nominationSkills.html.twig', [
            "form" => $form->createView(),
            "id" => $id,
            'stepNumber' => 3
        ]);
    }

    /**
     * @Route(path="/nomination/{id}/5", name="nomination_lang")
     */
    public function nominationLangs(Request $request, int $id): Response
    {
        $session= $request->getSession();
        $nominationArray= $session->get("nomination", []);
        if(!key_exists("skills", $nominationArray)){
            $this->addFlash("warning", "Veuillez suivre le processus de candidature et remplir les étapes une par une");
            return $this->redirectToRoute("nomination_personnal_info", ["id" => $id]);
        }
        if (!isset($nominationArray["langs"])){
            $nominationArray["langs"] = [];
            $session->set("nomination", $nominationArray);
        }
        $form= $this->createForm(LangsCollectionFormType::class, $nominationArray);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $nominationArray["langs"] = $form->get("langs")->getData();
            $session->set("nomination", $nominationArray);
            return $this->redirectToRoute("nomination_jobs", ["id" => $id]);
        }
        return $this->render('nomination/nominationLangs.html.twig', [
            "form" => $form->createView(),
            "id" => $id,
            'stepNumber' => 3
        ]);
    }



    /**
     * @Route(path="/nomination/{id}/6", name="nomination_jobs")
     */
    public function nominationJobs(Request $request, int $id): Response
    {
        $session= $request->getSession();
        $nominationArray= $session->get("nomination", []);
        if(!key_exists("langs", $nominationArray)){
            $this->addFlash("warning", "Veuillez suivre le processus de candidature et remplir les étapes une par une");
            return $this->redirectToRoute("nomination_personnal_info", ["id" => $id]);
        }
        if (!isset($nominationArray["jobs"])){
            $nominationArray["jobs"] = [];
            $session->set("nomination", $nominationArray);
        }
        $form= $this->createForm(JobsCollectionFormType::class, $nominationArray);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $nominationArray["jobs"] = $form->get("jobs")->getData();
            $session->set("nomination", $nominationArray);
            return $this->redirectToRoute("nomination_goals_info", ["id" => $id]);
        }
        return $this->render('nomination/nominationJobs.html.twig', [
            "form" => $form->createView(),
            "id" => $id,
            'stepNumber' => 4
        ]);
    }

    /**
     * @Route(path="/nomination/{id}/7", name="nomination_goals_info")
     */
    public function nominationGoalsInfo(Request $request, int $id): Response
    {
        $session= $request->getSession();
        $nominationArray= $session->get("nomination", []);
        if(!key_exists("jobs", $nominationArray)){
            $this->addFlash("warning", "Veuillez suivre le processus de candidature et remplir les étapes une par une");
            return $this->redirectToRoute("nomination_personnal_info", ["id" => $id]);
        }
        $goalsInfo = new GoalsInfo();
        $form = $this->createForm(GoalsInfoFormType::class, $goalsInfo);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $nominationArray["goalsInfo"] = $goalsInfo;
            $session->set("nomination", $nominationArray);
            return $this->redirectToRoute("nomination_aw_data", ["id" => $id]);
        }
        return $this->render('nomination/nominationGoalsInfos.html.twig', [
            "form" => $form->createView(),
            'stepNumber' => 5
        ]);
    }

    /**
     * @Route(path="/nomination/{id}/8", name="nomination_aw_data")
     */
    public function nominationAwData(Request $request, int $id): Response
    {
        $session= $request->getSession();
        $nominationArray= $session->get("nomination", []);
        if(!key_exists("goalsInfo", $nominationArray)){
            $this->addFlash("warning", "Veuillez suivre le processus de candidature et remplir les étapes une par une");
            return $this->redirectToRoute("nomination_personnal_info", ["id" => $id]);
        }
        $awData = new AwData();
        $form = $this->createForm(AwDataFormType::class, $awData);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $nominationArray["awData"] = $awData;
            $session->set("nomination", $nominationArray);
            return $this->redirectToRoute("nomination_end", ["id" => $id]);
        }
        return $this->render('nomination/nominationAwData.html.twig', [
            "form" => $form->createView(),
            'stepNumber' => 6
        ]);
    }

    /**
     * @Route(path="/nomination/{id}/end", name="nomination_end")
     */
    public function nominationEnd(Request $request, int $id, EntityManagerInterface $manager, MailService $mail): Response
    {
        $session= $request->getSession();
        $nominationArray= $session->get("nomination", []);
        if(!key_exists("awData", $nominationArray) || !key_exists("nominationName", $nominationArray)){
            $this->addFlash("warning", "Veuillez suivre le processus de candidature et remplir les étapes une par une");
            return $this->redirectToRoute("nomination_personnal_info", ["id" => $id]);
        }
        $nomination = new Nomination();
        $user = $manager->getRepository(User::class)->findOneBy(["email" => $this->getUser()->getUserIdentifier()]);
        $nomination->setName($nominationArray["nominationName"])
            ->setPersonnalInfos($nominationArray["personnalInfos"])
            ->setGoalsInfo($nominationArray["goalsInfo"])
            ->setSituation($nominationArray["situation"])
            ->setAwData($nominationArray["awData"])
            ->setValidationState(1)
            ->addMultipleDiploma($nominationArray["diplomas"])
            ->addMultipleSkills($nominationArray["skills"])
            ->addMultipleJob($nominationArray["jobs"])
            ->addMultipleLang($nominationArray["langs"])
            ->setUser($user);
        try {
            $manager->persist($nomination);
            $manager->flush();
            $session->remove("nomination");
            $this->addFlash("success", "Votre candidature à bien été envoyée");
            if (!$mail->sendNominationNotificationMail(
                $user->getEmail(),
                $user->getFirstname(),
                $user->getLastname(),
                $nomination->getName()
            )){
                throw new \Exception("Confirmation mail has not be sended");
            }
            return $this->redirectToRoute("home");
        } catch (\Exception $error){
            $session->remove("nomination");
            $this->addFlash("error", "Une erreur est survenue lors de votre candidature veuillez contacter l'administrateur");
            return $this->redirectToRoute("formation_item", [
                "id" => $id
            ]);
        }
    }

}
