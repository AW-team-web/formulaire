<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogueController extends AbstractController
{
    /**
     * @Route("/catalogue", name="app_catalogue")
     */
    public function allCourseCatalog(CategoryRepository $catrepo, CourseRepository $courseRepo): Response
    {
        try {
            $categories = $catrepo->findAll();
            $courses = $courseRepo->findAll();
            return $this->render('catalogue/index.html.twig', [
                'categories' => $categories,
                'courses' => $courses
            ]);
        } catch (\Exception $ex){
            //TODO implements logging system
            return $this->render('catalogue/index.html.twig', [
                'categories' => [],
                'courses' => []
            ]);
        }
    }

    /**
     * @Route("/catalogue/{cat_id}", name="app_catalogue_cat_id")
     */
    public function CourseCatalogById(CategoryRepository $catrepo, int $cat_id, CategoryRepository $courseRepo): Response
    {
        try {
            $categories = $catrepo->findAll();
            $activeCat = $catrepo->findOneBy(["id" => $cat_id]);
            $courses = $activeCat->getFormations();
            return $this->render('catalogue/index.html.twig', [
                'categories' => $categories,
                'active_cat' => $activeCat,
                'courses' => $courses
            ]);
        } catch (\Exception $ex){
            //TODO implements logging system
            return $this->render('catalogue/index.html.twig', [
                'categories' => [],
                'active_cat' => null,
                'courses' => []
            ]);
        }
    }
}
