<?php

namespace App\Controller\Admin;

use App\Admin\FileField;
use App\Entity\Course;
use App\Form\CourseModuleForm;
use App\Form\PaymentTypeForm;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CourseCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Course::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setPageTitle(Crud::PAGE_INDEX, "Liste des formations")
            ->setEntityLabelInSingular("Formation")
            ->setEntityLabelInPlural("Formations")
            ->setPageTitle(Crud::PAGE_NEW, "Créer une formation");
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions;
    }

    public function configureAssets(Assets $assets): Assets
    {
       return $assets
           ->addWebpackEncoreEntry("course_crud_edit");
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', "ID")
                    ->hideOnForm(),
            TextField::new('name', "Nom")
                ->setPermission("ROLE_ADMIN"),
            TextField::new("menu_name", "Nom dans le menu"),
            FileField::new('course_documentation', "Brochure")
                ->setUploadDir("uploads/course_doc/"),
            AssociationField::new('category', 'Catégorie'),
            TextField::new('course_duration', 'Durée du cours')
                ->hideOnIndex(),
            ChoiceField::new('course_duration_unit', 'Type de durée')
                ->setChoices([
                    "Heure" => "heure",
                    "Heures" => "heures",
                    "Jour" => "jour",
                    "Jours" => "jours",
                    "Semaine" => "semaine",
                    "Semaines" => "semaines",
                    "Mois" => "mois"
                ])
                ->hideOnIndex(),
            Field::new('course_end_level', 'Niveau de sortie')
                ->hideOnIndex(),
            TextField::new('course_professor', 'Enseignant référant'),
            BooleanField::new('page_created', 'Lié à une formation'),
            TextEditorField::new('course_short_description', 'Description courte de la formation')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            TextEditorField::new('course_long_description', 'Description longue du cours')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            TextField::new('course_public', 'Public de destination de la formation')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            TextField::new('course_marketing_prerequisite', 'Pré-requis apparaissant dans la présentation de la formation')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            IntegerField::new('course_student_number', 'Nombre d\'étudiants')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            ChoiceField::new('course_format', 'Type de suivi de la formation')
                ->setChoices([
                    "Distanciel" => "distanciel",
                    "Présentiel" => "présentiel",
                    "Distanciel et présentiel" => "distanciel et présentiel"
                ])
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            TextField::new('course_end_title', 'Titre ou diplôme en fin de formation')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            TextField::new('course_availability', 'Rentrées sous forme de texte')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            ArrayField::new('course_in_progress_skills', 'Compétences acquérie en cours de formation')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            // ArrayField::new('course_end_skills', 'Compétences en fin de formation')
            ArrayField::new('course_end_skills', 'Dates et lieux')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            // ArrayField::new('course_activities', 'Compétences nécessaires pour réaliser les activités')
            ArrayField::new('course_activities', 'Suivi et évaluation')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            ArrayField::new('course_end_goals', 'Moyens pédago')
            // ArrayField::new('course_end_goals', 'But de la formation: diplôme, certificat etc...')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            ArrayField::new('course_end_jobs', 'Métier pouvant être exercé à la fin de la formation')
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            CollectionField::new('course_pricing', 'Méthodes de paiement de la formation')
                ->setEntryType(PaymentTypeForm::class)
                ->setEntryIsComplex(true)
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            CollectionField::new('course_modules', 'Modules composant la formation')
                ->setEntryType(CourseModuleForm::class)
                ->setEntryIsComplex(true)
                ->hideOnIndex()
                ->addCssClass('course-edit'),
            ArrayField::new('course_prerequisites', 'Liste complète des prés-requis de la formation')
                ->hideOnIndex()
                ->addCssClass('course-edit')
        ];
    }
}
