<?php

namespace App\Controller\Admin;

use App\Admin\DiplomaField;
use App\Admin\JobField;
use App\Admin\LangField;
use App\Admin\SkillField;
use App\Entity\Nomination;
use App\Services\MailService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class NominationCrudController extends AbstractCrudController
{
    private AdminUrlGenerator $urlGenerator;

    public static function getEntityFqcn(): string
    {
        return Nomination::class;
    }

    public function __construct(AdminUrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular("Candidature")
            ->setEntityLabelInPlural("Candidatures")
            ->overrideTemplate("crud/detail", "admin/pages/nomination_details.html.twig");
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new("user", "Candidat"),
            TextField::new('name', "Nom de la formation")
            ->hideOnForm(),
            FormField::addPanel("Informations du candidat")
                ->onlyOnDetail(),
            DateField::new("personnalInfos.birthDate", "Date de naissance")
                ->onlyOnDetail(),
            TextField::new("personnalInfos.birthPlace", "Lieu de naissance")
                ->onlyOnDetail(),
            TextField::new("personnalInfos.nationality", "Nationalité du candidat")
                ->onlyOnDetail(),
            NumberField::new("personnalInfos.nationalSecNumber", "Numéro de sécurité sociale")
                ->onlyOnDetail(),
            TextField::new("personnalInfos.familyState", "Statut familial")
                ->onlyOnDetail(),
            BooleanField::new("personnalInfos.drivingLicence", "Permis de conduire")
                ->onlyOnDetail(),
            FormField::addPanel("Situation Actuelle du candidat")
                ->onlyOnDetail(),
            TextField::new("situation.title", "Situation actuelle")
                ->onlyOnDetail(),
            DateField::new("situation.beginningDate", "Depuis le")
                ->onlyOnDetail(),
            FormField::addPanel("Objectifs du candidat")
                ->onlyOnDetail(),
            BooleanField::new("goalsInfo.xpIntoDomain", "Le candidat a de l'experience dans le domaine")
                ->onlyOnDetail(),
            BooleanField::new("goalsInfo.expertMet", "Le candidat a rencontré un expert")
                ->onlyOnDetail(),
            TextareaField::new("goalsInfo.qualityPoints", "Qualitées du candidat")
                ->onlyOnDetail(),
            TextareaField::new("goalsInfo.weakpoints", "Points faible du candidat")
                ->onlyOnDetail(),
            TextareaField::new("goalsInfo.professionnalProject", "Projet professionnel du candidat")
                ->onlyOnDetail(),
            TextareaField::new("goalsInfo.typeOfCompanyWanted", "Type d'entreprise visée par le candidat")
                ->onlyOnDetail(),
            FormField::addPanel("Connaissance d'Apprenti-webeur")
                ->onlyOnDetail(),
            TextField::new("awData.awKnowledge", "Par quel moyen le candidat a-t-il connu Apprenti-webeur")
                ->onlyOnDetail(),
            BooleanField::new("awData.otherTrainingFailed", "Le candidat a-t-il deja échoué sur une de nos formations")
                ->onlyOnDetail(),
            TextField::new("awData.trainingType", "Si oui laquelle")
                ->onlyOnDetail(),
            FormField::addPanel("Diplômes")
            ->onlyOnDetail(),
            DiplomaField::new("diplomas")
            ->onlyOnDetail(),
            FormField::addPanel("Expérience")
            ->onlyOnDetail(),
            JobField::new("jobs")
            ->onlyOnDetail(),
            FormField::addPanel("Compétences")
            ->onlyOnDetail(),
            SkillField::new("skills")
            ->onlyOnDetail(),
            FormField::addPanel("Langues pratiquées")
            ->onlyOnDetail(),
            LangField::new("langs")
            ->onlyOnDetail(),
            FormField::addPanel("Etat de la validation")
            ->onlyOnDetail(),
            NumberField::new('validationState', "Etat de la candidature")
            ->formatValue(function ($value){
                switch ($value){
                    case 1:
                        return "En attente";
                    case 2:
                        return "Validée";
                    case 3:
                        return "Refusée";
                    default:
                        return "Inconnu";
                }
            })
            ->hideOnForm(),
            TextField::new("validator", "Validateur")
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $validNomination = Action::new("validNomination", "Valider")
            ->linkToCrudAction("validNomination")
            ->addCssClass("text-success btn");
        $denyNomination = Action::new("denyNomination", "Refuser")
            ->linkToCrudAction("denyNomination")
            ->addCssClass("text-warning btn");
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::EDIT)
            ->disable(Action::NEW)
            ->add(Crud::PAGE_DETAIL, $validNomination)
            ->setPermission(Action::DELETE, "ROLE_ADMIN")
            ->add(Crud::PAGE_DETAIL, $denyNomination);
    }

    public function validNomination(AdminContext $context, LoggerInterface $logger, MailService $mail):Response{
        try {
            $nomination = $context->getEntity()->getInstance();
            $validator = $context->getUser();
            if ($nomination ==! null){
                $nomination->setValidationState(2);
                $nomination->setValidator($validator->getUserIdentifier());
                $this->persistEntity($this->container->get('doctrine')->getManagerForClass($context->getEntity()->getFqcn()), $nomination);
                $url = $this->urlGenerator
                    ->setController(NominationCrudController::class)
                    ->setAction(Action::INDEX)
                    ->generateUrl();
                $nominatedUser = $nomination->getUser();
                if(!$mail->sendNominationConfirmMail(
                    $nominatedUser->getEmail(),
                    $nominatedUser->getFirstname(),
                    $nominatedUser->getLastname(),
                    $nomination->getName()
                )){
                    return $this->getRedirectResponseAfterSave($context, Action::DETAIL);
                }
                return $this->redirect($url);
            } else {
                return $this->getRedirectResponseAfterSave($context, Action::DETAIL);
            }
        } catch(\Exception $e){
            $logger->error($e);
            return $this->redirect(
                $this->urlGenerator
                ->setController(NominationCrudController::class)
                ->setAction(ACTION::DETAIL)
                ->generateUrl()
            );
        }
    }

    public function denyNomination(AdminContext $context, LoggerInterface $logger, MailService $mail):Response{
        try {
            $nomination = $context->getEntity()->getInstance();
            if ($nomination ==! null){
                $nomination->setValidationState(3);
                $nomination->setValidator(null);
                $this->persistEntity($this->container->get('doctrine')->getManagerForClass($context->getEntity()->getFqcn()), $nomination);
                $url = $this->urlGenerator
                    ->setController(NominationCrudController::class)
                    ->setAction(Action::INDEX)
                    ->generateUrl();
                $nominatedUser = $nomination->getUser();
                if(!$mail->sendNominationRejectMail(
                    $nominatedUser->getEmail(),
                     $nominatedUser->getFirstname(),
                    $nominatedUser->getLastname(),
                    $nomination->getName()
                )){
                    return $this->getRedirectResponseAfterSave($context, Action::DETAIL);
                }
                return $this->redirect($url);
            } else {
                return $this->getRedirectResponseAfterSave($context, Action::DETAIL);
            }
        } catch (\Exception $e){
            $logger->error($e);
            return $this->redirect(
                $this->urlGenerator
                    ->setController(NominationCrudController::class)
                    ->setAction(ACTION::DETAIL)
                    ->generateUrl()
            );
        }
    }
}
