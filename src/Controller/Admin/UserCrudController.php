<?php

namespace App\Controller\Admin;

use App\Admin\AdressField;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular("Utilisateur")
            ->setEntityLabelInPlural("Utilisateurs")
            ->setPageTitle(Crud::PAGE_NEW, "Créer un utilisateur")
            ->overrideTemplate("crud/new", "admin/pages/user_new.html.twig")
            ->overrideTemplate("crud/edit", "admin/pages/user_edit.html.twig");
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new("firstname", "Prénom"),
            Field::new("lastname", "Nom"),
            EmailField::new("email", "Mail"),
            TextField::new("password", "Mot de passe")
                ->onlyOnForms()
                ->hideWhenUpdating(),
            TelephoneField::new("phone", "Telephone"),
            AdressField::new("Adress")
                ->onlyOnForms()
                ->setLabel(false),
            TextField::new("Adress.address", "Adresse")->onlyOnDetail(),
            Field::new("Adress.zipcode", "Code postal")->onlyOnDetail(),
            TextField::new("Adress.city", "Ville")->onlyOnDetail(),
            TextField::new("Adress.country", "Pays")->onlyOnDetail(),
            ChoiceField::new("roles", "Statut")
                ->setChoices(["Candidat" => "ROLE_CANDIDAT", "Formateur" => "ROLE_FORMATEUR", "Admin" => "ROLE_ADMIN"])
                ->allowMultipleChoices()
                ->onlyOnForms(),
            Field::new("roles[0]", "Statut")
            ->hideOnForm()
            ->formatValue(function ($value) {
                switch ($value){
                    case "ROLE_ADMIN":
                        return "Admin";
                    case "ROLE_FORMATEUR":
                        return "Formateur";
                    case "ROLE_CANDIDAT":
                        return "Candidat";
                    default:
                        return "Utilisateur";
                }
            }),
            NumberField::new("nominationNumber", "Nombre de candidatures")
            ->onlyOnDetail()


        ];

    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }




    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
