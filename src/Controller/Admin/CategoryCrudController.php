<?php

namespace App\Controller\Admin;

use App\Admin\FileField;
use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Category::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setPageTitle(Crud::PAGE_INDEX, "Liste des catégories")
            ->setEntityLabelInSingular("Catégorie")
            ->setEntityLabelInPlural("Catégories")
            ->setPageTitle(Crud::PAGE_NEW, "Créer une catégorie");
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', "ID")->hideOnForm(),
            TextField::new('name', "Nom"),
            FileField::new("img_url", "Image")
            ->setUploadDir("/uploads/category_img")
        ];
    }

}
