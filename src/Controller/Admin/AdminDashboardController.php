<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Course;
use App\Entity\Nomination;
use App\Entity\Service;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractDashboardController
{

    private AdminUrlGenerator $urlGenerator;

    public function __construct(AdminUrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $urlFormateur = $this->urlGenerator
            ->setController(NominationCrudController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();
        $urlAdmin = $this->urlGenerator
            ->setController(UserCrudController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();
        if ($this->isGranted("ROLE_ADMIN")){
            return $this->redirect($urlAdmin);
        } else {
            return $this->redirect($urlFormateur);
        }

    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Apprenti webeur')
            ->renderContentMaximized(true);
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute("Retour sur le site", "fas fa-home", "home");
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class)
            ->setPermission("ROLE_ADMIN");
        yield MenuItem::linkToCrud('Candidatures', 'fas fa-paperclip', Nomination::class);
        yield MenuItem::linkToCrud('Liste des Formations', "fas fa-graduation-cap", Course::class);
        yield MenuItem::linkToCrud('Catégories', 'fas fa-list', Category::class);
        yield MenuItem::linkToCrud('Services', 'fas fa-list', Service::class);
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
