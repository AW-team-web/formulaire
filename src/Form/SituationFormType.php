<?php

namespace App\Form;

use App\Entity\Situation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SituationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', ChoiceType::class, [
                "label" => "Votre statut",
                "choices" => [
                    "Sans emploi" => "Sans emploi",
                    "Salarié(e)" => "Salarié(e)",
                    "Etudiant(e)" => "Etudiant(e)",
                ]
            ])
            ->add('beginningDate', DateType::class, [
                "label" => "Depuis le : ",
                "format" => "ddMMMMyyyy",
                "model_timezone" => "Europe/paris",
                "view_timezone" => "Europe/Paris",
                "years" => range(date("Y") - 77, date('Y'))
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Situation::class,
        ]);
    }
}
