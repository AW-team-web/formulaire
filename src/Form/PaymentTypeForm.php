<?php

namespace App\Form;


use App\Entity\PaymentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentTypeForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', TextType::class, [
                "label" => "Type"
            ])
            ->add('subtitle', TextType::class, [
                "label" => "Sous-titre"
            ])
            ->add('amount', TextType::class, [
                "label" => "Montant"
            ])
            ->add('unit', ChoiceType::class, [
                "label" => "Unité",
                "choices" => [
                    "Euros" => "€",
                    "Pourcentage" => "%",
                    "Facturation Mensuelle" => "€/mois",
                    "Facturation Annuelle" => "€/an"
                ]
            ])
            ->add('description', TextType::class, [
                "label" => "Description"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            "data_class" => PaymentType::class
        ]);
    }
}
