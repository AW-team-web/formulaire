<?php

namespace App\Form;

use App\Entity\CourseModule;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CourseModuleForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('module_name', TextType::class, [
                "label" => "Nom"
            ])
            ->add('module_items', CollectionType::class, [
                "label" => "Élements",
                "entry_options" => [
                    "label" => false,
                ],
                "entry_type" => TextType::class,
                "allow_delete" => true,
                "allow_add" => true,
                "prototype_name" => "__item_name__"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            "data_class" => CourseModule::class
        ]);
    }
}
