<?php

namespace App\Form;

use App\Entity\User;
use Doctrine\DBAL\Types\StringType;
use http\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\EmailValidator;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                "required" => true,
                "label" => "Email",
                "constraints" => [
                    new NotBlank([
                        "message" => "Votre email doit etre renseigné"
                    ])
                ]
            ])
            ->add('password', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Votre mot de passe doit etre renseigné',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir au minimum {{ limit }} caracteres',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
                'label' => 'Mot de passe'
            ])
            ->add("firstname", TextType::class, [
                "required" => true,
                "label" => "Prénom",
                "constraints" => [
                    new NotBlank([
                        "message" => "Vous devez renseigner votre prenom"
                    ])
                ]
            ])
            ->add("lastname", TextType::class, [
                "required" => true,
                "label" => "Nom",
                "constraints" => [
                    new NotBlank([
                        "message" => "Vous devez renseigner votre nom"
                    ])
                ]
            ])
            ->add("adress", AdressType::class)
            ->add("phone", TelType::class, [
                "required" => true,
                "label" => "Téléphone",
                "constraints" => [
                    new NotBlank([
                        "message" => "Votre numéro de téléphone est obligatoire. "
                    ]),
                    new Length([
                       "max" => 15,
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
