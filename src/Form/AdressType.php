<?php

namespace App\Form;

use App\Entity\Adress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('city', TextType::class, [
                "required" => true,
                "label" => "Ville"
            ])
            ->add('country', TextType::class, [
                "required" => true,
                "label" => "Pays"
            ])
            ->add('zipcode', IntegerType::class, [
                "required" => true,
                "label" => "Code postal"
            ])
            ->add('address', TextType::class, [
                "required" => true,
                "label" => "Votre adresse"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Adress::class,
        ]);
    }

}
