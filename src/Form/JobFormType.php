<?php

namespace App\Form;

use App\Entity\Job;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('jobTitle', TextType::class, [
                "label" => "Poste occupé",
                "required" => true
            ])
            ->add('beginingDate', DateType::class, [
                "label" => "Date de début",
                "required" => true,
                "years" => range(date('Y') - 77, date('Y'))
            ])
            ->add("stayTime", DateIntervalType::class, [
                "label" => false,
                "labels" => [
                    "years" => "Ancienneté"
                ],
                "placeholder" => "Saisissez votre ancienneté en année",
                "input" => "string",
                "with_days" => false,
                "with_months" => false,
                "required" => true,
            ])
            ->add('companyName', TextType::class, [
                "label" => "Nom de votre employeur",
                "required" => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Job::class,
        ]);
    }
}
