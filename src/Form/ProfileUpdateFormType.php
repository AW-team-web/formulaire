<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfileUpdateFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                "required" => true,
                "label" => "Email",
                "constraints" => [
                    new NotBlank([
                        "message" => "Votre email doit etre renseigné"
                    ])
                ]
            ])
            ->add("firstname", TextType::class, [
                "required" => true,
                "label" => "Prénom",
                "constraints" => [
                    new NotBlank([
                        "message" => "Vous devez renseigner votre prenom"
                    ])
                ]
            ])
            ->add("lastname", TextType::class, [
                "required" => true,
                "label" => "Nom",
                "constraints" => [
                    new NotBlank([
                        "message" => "Vous devez renseigner votre nom"
                    ])
                ]
            ])
            ->add("adress", AdressType::class)
            ->add("phone", TelType::class, [
                "required" => true,
                "label" => "Téléphone",
                "constraints" => [
                    new NotBlank([
                        "message" => "Votre numéro de téléphone est obligatoire. "
                    ])
                ]
            ])
            ->add("update", SubmitType::class, [
                "attr" => ["class" => "btn"],
                "label" => "Mettre à jour mon profil"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
