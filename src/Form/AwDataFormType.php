<?php

namespace App\Form;

use App\Entity\AwData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AwDataFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('awKnowledge', ChoiceType::class, [
                "label" => "Comment avez-vous connu apprenti-webeur ?",
                "required" => true,
                "choices" => [
                    "Internet" => "Internet",
                    "Télévision" => "Télévision",
                    "Proches" => "Proches",
                    "Site d'emploi" => "Site d'emploi"
                ]
            ])
            ->add('otherTrainingFailed', ChoiceType::class, [
                "label" => "Avez-vous déja participé à une de nos formations",
                "choices" => [
                    "Oui" => true,
                    "Non" => false
                ],
                "required" => true
            ])
            ->add('trainingType', ChoiceType::class, [
                "label" => "Si oui laquelle ?",
                "choices" => [
                    "" => null,
                    "Technicien systemes et reseaux" => "TSSR",
                    "Wordpress" => "wordpress",
                    "Web Entrepreneur" => "web_entrepreneur"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AwData::class,
        ]);
    }
}
