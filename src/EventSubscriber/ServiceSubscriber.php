<?php

namespace App\EventSubscriber;

use App\Entity\Service;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\String\Slugger\SluggerInterface;

class ServiceSubscriber implements EventSubscriber
{
    private SluggerInterface $slugger;

    /**
     * @param SluggerInterface $slugger
     */
    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }


    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void{
        $service = $args->getEntity();
        if($service instanceof Service){
            $service->setSlug($this->slugger->slug($service->getName()));
        }
    }

    public function preUpdate(LifecycleEventArgs $args): void{
        $service = $args->getEntity();
        if($service instanceof Service){
            $service->setSlug($this->slugger->slug($service->getName()));
        }
    }
}
