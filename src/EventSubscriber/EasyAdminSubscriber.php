<?php

namespace App\EventSubscriber;

use App\Entity\Course;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private SluggerInterface $slugger;

    /**
     * @param SluggerInterface $slugger
     */
    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }


    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => 'setCourseSlugOnPersist',
            BeforeEntityUpdatedEvent::class => 'setCourseSlugOnUpdate'
        ];
    }

    public function setCourseSlugOnPersist(BeforeEntityPersistedEvent $event)
    {
       $course = $event->getEntityInstance();
       if (!$course instanceof Course){
           return;
       }
       $course->setSlug($this->slugger->slug($course->getName()));
    }

    public function setCourseSlugOnUpdate(BeforeEntityUpdatedEvent $event)
    {
        $course = $event->getEntityInstance();
        if (!$course instanceof Course){
            return;
        }
        $course->setSlug($this->slugger->slug($course->getName()));
    }
}
