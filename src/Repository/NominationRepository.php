<?php

namespace App\Repository;

use App\Entity\Nomination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Nomination|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nomination|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nomination[]    findAll()
 * @method Nomination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NominationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Nomination::class);
    }

    /**
     * Count the number of nomination in queue for being confirmed
     * @param UserInterface $user
     * @return int|mixed|string
     */
    public function getWaitingNominationNumber(UserInterface $user){
        try {
            return $this->createQueryBuilder('n')
                ->select('COUNT(n)')
                ->where('n.user = :user')
                ->setParameter('user', $user)
                ->andWhere('n.validationState = :state')
                ->setParameter('state', 1)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (\Exception $e){
            return null;
        }
    }

    // /**
    //  * @return Nomination[] Returns an array of Nomination objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Nomination
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
