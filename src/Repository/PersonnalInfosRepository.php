<?php

namespace App\Repository;

use App\Entity\PersonnalInfos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PersonnalInfos|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonnalInfos|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonnalInfos[]    findAll()
 * @method PersonnalInfos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonnalInfosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonnalInfos::class);
    }

    // /**
    //  * @return PersonnalInfos[] Returns an array of PersonnalInfos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PersonnalInfos
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
