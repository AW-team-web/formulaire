<?php

namespace App\Repository;

use App\Entity\AwData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AwData|null find($id, $lockMode = null, $lockVersion = null)
 * @method AwData|null findOneBy(array $criteria, array $orderBy = null)
 * @method AwData[]    findAll()
 * @method AwData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AwDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AwData::class);
    }

    // /**
    //  * @return AwData[] Returns an array of AwData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AwData
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
