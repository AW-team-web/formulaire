<?php

namespace App\Admin\FieldConfigurator;

use App\Admin\FileField;
use App\Entity\Course;
use App\Kernel;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldConfiguratorInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use Symfony\Component\Filesystem\Filesystem;
use function Symfony\Component\String\u;

class FileFieldConfigurator implements FieldConfiguratorInterface
{
    private Kernel $kernel;
    private Filesystem $fs;

    /**
     * @param Kernel $kernel
     */
    public function __construct(Kernel $kernel, Filesystem $fs)
    {
        $this->kernel = $kernel;
        $this->fs = $fs;
    }


    public function supports(FieldDto $field, EntityDto $entityDto): bool
    {
        return FileField::class === $field->getFieldFqcn();
    }

    public function configure(FieldDto $field, EntityDto $entityDto, AdminContext $context): void
    {
        if ($entityDto->getInstance() instanceof Course){
            /**
             * @var $course Course
             */
            $course = $entityDto->getInstance();
            $courseDoc = $course->getCourseDocumentation();
            if (!$courseDoc){
                $field->setFormattedValue("Aucun(e)");
            } else {
                $field->setFormattedValue($field->getValue());
            }
        }

        $field->setFormTypeOption('multiple', false);

        $field->setFormTypeOption('upload_filename', $field->getCustomOption(FileField::OPTION_UPLOADED_FILE_NAME_PATTERN));

        if (!\in_array($context->getCrud()->getCurrentPage(), [Crud::PAGE_EDIT, Crud::PAGE_NEW], true)) {
            return;
        }

        $relativeUploadDir = $field->getCustomOption(FileField::OPTION_UPLOAD_DIR);
        if (null === $relativeUploadDir) {
            throw new \InvalidArgumentException(sprintf('The "%s" image field must define the directory where the images are uploaded using the setUploadDir() method.', $field->getProperty()));
        }

        $relativeUploadDir = u($relativeUploadDir)->trimStart(\DIRECTORY_SEPARATOR)->ensureEnd(\DIRECTORY_SEPARATOR)->toString();
        $absoluteUploadDir = u("public/".$relativeUploadDir)->ensureStart($this->kernel->getProjectDir().\DIRECTORY_SEPARATOR)->toString();

        if (!is_dir($absoluteUploadDir)){
            $this->fs->mkdir($relativeUploadDir) ?? $field->setFormTypeOption('upload_dir', $absoluteUploadDir);
        } else {
            $field->setFormTypeOption('upload_dir', $absoluteUploadDir);
        }

    }


}