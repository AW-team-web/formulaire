<?php

namespace App\Admin;

use App\Form\AdressType;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;

class DiplomaField implements FieldInterface
{
    use FieldTrait;

    /**
     * @param string|false|null $label
     */
    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel(false)
            ->setTemplatePath("admin/fields/diploma_field.html.twig")
            ->setCustomOption("fieldType", "relationField");

    }
}