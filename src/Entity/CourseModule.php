<?php

namespace App\Entity;

use App\Repository\CourseModuleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CourseModuleRepository::class)
 * Module du programme composant un cours.
 */
class CourseModule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $module_name;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $module_items;

    /**
     * @ORM\ManyToOne(targetEntity=Course::class, inversedBy="course_modules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getModuleName()
    {
        return $this->module_name;
    }

    /**
     * @param mixed $module_name
     */
    public function setModuleName($module_name): void
    {
        $this->module_name = $module_name;
    }

    public function __toString()
    {
        return $this->module_name;
    }


    public function getModuleItems(): array
    {
        return $this->module_items;
    }

    public function setModuleItems(array $module_items): void
    {
        $this->module_items = $module_items;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

}
