<?php

namespace App\Entity;

use App\Repository\AwDataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AwDataRepository::class)
 */
class AwData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $awKnowledge;

    /**
     * @ORM\Column(type="boolean")
     */
    private $otherTrainingFailed;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $trainingType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAwKnowledge(): String
    {
        return $this->awKnowledge;
    }

    public function setAwKnowledge(String $awKnowledge): self
    {
        $this->awKnowledge = $awKnowledge;

        return $this;
    }

    public function getOtherTrainingFailed(): ?bool
    {
        return $this->otherTrainingFailed;
    }

    public function setOtherTrainingFailed(bool $otherTrainingFailed): self
    {
        $this->otherTrainingFailed = $otherTrainingFailed;

        return $this;
    }

    public function getTrainingType(): ?string
    {
        return $this->trainingType;
    }

    public function setTrainingType(string $trainingType): self
    {
        $this->trainingType = $trainingType;

        return $this;
    }
}
