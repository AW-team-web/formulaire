<?php

namespace App\Entity;

use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CourseRepository::class)
 * Entité représentant un cours.
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $menuName;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $course_documentation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * Description courte du cours
     */
    private $course_short_description;

    /**
     * @ORM\Column(type="text", nullable=true)
     * Description longue du cours
     */
    private $course_long_description;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * Public de destination de la formation
     */
    private $course_public;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * Pré-requis apparaissant dans la presentation de la formation
     */
    private $course_marketing_prerequisite;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * Nombre d'étudiants dans la formation
     */
    private $course_student_number;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex(
     *     pattern="/[a-zA-Z]/",
     *     match=false,
     *     message="Seuls les chiffres doivent être utilisés avec ',' ou '.'"
     * )
     * Durée de la formation
     */
    private $course_duration;

    /**
     * @ORM\Column(type="string", nullable=true, length=100)
     * Unité pour la durée de la formation
     */
    private $course_duration_unit;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * Niveau en fin de formation
     */
    private $course_end_level;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * Type de suivi de la formation: présentiel, distanciel, les deux
     */
    private $course_format;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * Titre ou diplôme en fin de formation
     */
    private $course_end_title;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * Rentrées sous forme de texte
     */
    private $course_availability;

    /**
     * @ORM\Column(type="json", nullable=true)
     * Compétences acquérie en cours de formation
     */
    private $course_in_progress_skills;

    /**
     * @ORM\Column(type="json", nullable=true)
     * Compétences en fin de formation
     */
    private $course_end_skills;

    /**
     * @ORM\Column(type="json", nullable=true)
     *Compétences nécessaires pour réaliser les activités
     */
    private $course_activities;

    /**
     * @ORM\Column(type="json", nullable=true)
     * But de la formation: diplôme, certificat etc...
     */
    private $course_end_goals;

    /**
     * @ORM\Column(type="json", nullable=true)
     * Métier pouvant être exercé à la fin de la formation
     */
    private $course_end_jobs;

    /**
     * @ORM\Column(type="json", nullable=true)
     * Liste complète des prés-requis de la formation
     */
    private $course_prerequisites;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="formations")
     */
    private $category;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $page_created;

    /**
     * @ORM\ManyToMany(targetEntity=PaymentType::class, cascade={"persist"}, orphanRemoval=true)
     * Méthode de paiement de la formation
     */
    private $course_pricing;

    /**
     * @ORM\OneToMany(targetEntity=CourseModule::class, mappedBy="course", orphanRemoval=true, cascade={"persist"})
     * Modules composant le cours
     */
    private $course_modules;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * Enseignant réferant
     */
    private $course_professor;


    public function __construct()
    {
        $this->course_pricing = new ArrayCollection();
        $this->course_modules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return ?String
     */
    public function getCourseDocumentation(): ?string
    {
        if (!$this->course_documentation) {
            return null;
        }
        if (strpos($this->course_documentation, "/") !== false) {
            return $this->course_documentation;
        }
        return $this->course_documentation;
    }

    /**
     * @param ?String $course_documentation
     */
    public function setCourseDocumentation(?string $course_documentation): void
    {
        $this->course_documentation = $course_documentation;
    }

    /**
     * @return mixed
     */
    public function getPageCreated()
    {
        return $this->page_created;
    }

    /**
     * @param mixed $page_created
     */
    public function setPageCreated($page_created): self
    {
        $this->page_created = $page_created;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseShortDescription()
    {
        return $this->course_short_description;
    }

    /**
     * @param mixed $course_short_description
     */
    public function setCourseShortDescription($course_short_description): self
    {
        $this->course_short_description = $course_short_description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseLongDescription()
    {
        return $this->course_long_description;
    }

    /**
     * @param mixed $course_long_description
     */
    public function setCourseLongDescription($course_long_description): self
    {
        $this->course_long_description = $course_long_description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoursePublic()
    {
        return $this->course_public;
    }

    /**
     * @param mixed $course_public
     */
    public function setCoursePublic($course_public): self
    {
        $this->course_public = $course_public;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseMarketingPrerequisite()
    {
        return $this->course_marketing_prerequisite;
    }

    /**
     * @param mixed $course_marketing_prerequisite
     */
    public function setCourseMarketingPrerequisite($course_marketing_prerequisite): self
    {
        $this->course_marketing_prerequisite = $course_marketing_prerequisite;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseStudentNumber()
    {
        return $this->course_student_number;
    }

    /**
     * @param mixed $course_student_number
     */
    public function setCourseStudentNumber($course_student_number): self
    {
        $this->course_student_number = $course_student_number;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseDuration()
    {
        return $this->course_duration;
    }

    /**
     * @param mixed $course_duration
     */
    public function setCourseDuration($course_duration): self
    {
        $this->course_duration = $course_duration;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseDurationUnit()
    {
        return $this->course_duration_unit;
    }

    /**
     * @param mixed $course_duration_unit
     */
    public function setCourseDurationUnit($course_duration_unit): self
    {
        $this->course_duration_unit = $course_duration_unit;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseEndLevel()
    {
        return $this->course_end_level;
    }

    /**
     * @param mixed $course_end_level
     */
    public function setCourseEndLevel($course_end_level): self
    {
        $this->course_end_level = $course_end_level;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseFormat()
    {
        return $this->course_format;
    }

    /**
     * @param mixed $course_format
     */
    public function setCourseFormat($course_format): self
    {
        $this->course_format = $course_format;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseEndTitle()
    {
        return $this->course_end_title;
    }

    /**
     * @param mixed $course_end_title
     */
    public function setCourseEndTitle($course_end_title): self
    {
        $this->course_end_title = $course_end_title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseAvailability()
    {
        return $this->course_availability;
    }

    /**
     * @param mixed $course_availability
     */
    public function setCourseAvailability($course_availability): self
    {
        $this->course_availability = $course_availability;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseInProgressSkills()
    {
        return $this->course_in_progress_skills;
    }

    /**
     * @param mixed $course_in_progress_skills
     */
    public function setCourseInProgressSkills($course_in_progress_skills): self
    {
        $this->course_in_progress_skills = $course_in_progress_skills;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseEndSkills()
    {
        return $this->course_end_skills;
    }

    /**
     * @param mixed $course_end_skills
     */
    public function setCourseEndSkills($course_end_skills): self
    {
        $this->course_end_skills = $course_end_skills;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseActivities()
    {
        return $this->course_activities;
    }

    /**
     * @param mixed $course_activities
     */
    public function setCourseActivities($course_activities): self
    {
        $this->course_activities = $course_activities;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseEndGoals()
    {
        return $this->course_end_goals;
    }

    /**
     * @param mixed $course_end_goals
     */
    public function setCourseEndGoals($course_end_goals): self
    {
        $this->course_end_goals = $course_end_goals;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCourseEndJobs()
    {
        return $this->course_end_jobs;
    }

    /**
     * @param mixed $course_end_jobs
     */
    public function setCourseEndJobs($course_end_jobs): self
    {
        $this->course_end_jobs = $course_end_jobs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoursePrerequisites()
    {
        return $this->course_prerequisites;
    }

    /**
     * @param mixed $course_prerequisites
     */
    public function setCoursePrerequisites($course_prerequisites): self
    {
        $this->course_prerequisites = $course_prerequisites;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMenuName()
    {
        return $this->menuName;
    }

    /**
     * @param mixed $menuName
     */
    public function setMenuName($menuName): self
    {
        $this->menuName = $menuName;

        return $this;
    }

    /**
     * @return Collection<int, PaymentType>
     */
    public function getCoursePricing(): Collection
    {
        return $this->course_pricing;
    }

    public function addCoursePricing(PaymentType $coursePricing): self
    {
        if (!$this->course_pricing->contains($coursePricing)) {
            $this->course_pricing[] = $coursePricing;
        }

        return $this;
    }

    public function removeCoursePricing(PaymentType $coursePricing): self
    {
        $this->course_pricing->removeElement($coursePricing);

        return $this;
    }

    /**
     * @return Collection<int, CourseModule>
     */
    public function getCourseModules(): Collection
    {
        return $this->course_modules;
    }

    public function addCourseModule(CourseModule $courseModule): self
    {
        if (!$this->course_modules->contains($courseModule)) {
            $this->course_modules[] = $courseModule;
            $courseModule->setCourse($this);
        }

        return $this;
    }

    public function removeCourseModule(CourseModule $courseModule): self
    {
        if ($this->course_modules->removeElement($courseModule)) {
            // set the owning side to null (unless already changed)
            if ($courseModule->getCourse() === $this) {
                $courseModule->setCourse(null);
            }
        }

        return $this;
    }

    public function getCourseProfessor(): ?string
    {
        return $this->course_professor;
    }

    public function setCourseProfessor(string $course_professor): self
    {
        $this->course_professor = $course_professor;

        return $this;
    }






}
