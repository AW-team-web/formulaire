<?php

namespace App\Entity;

use App\Repository\PersonnalInfosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonnalInfosRepository::class)
 */
class PersonnalInfos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", length=20)
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $birthPlace;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nationality;

    /**
     * @ORM\Column(type="integer")
     */
    private $nationalSecNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $familyState;

    /**
     * @ORM\Column(type="boolean")
     */
    private $drivingLicence;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    public function setBirthPlace(string $birthPlace): self
    {
        $this->birthPlace = $birthPlace;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getNationalSecNumber(): ?int
    {
        return $this->nationalSecNumber;
    }

    public function setNationalSecNumber(int $nationalSecNumber): self
    {
        $this->nationalSecNumber = $nationalSecNumber;

        return $this;
    }

    public function getFamilyState(): ?string
    {
        return $this->familyState;
    }

    public function setFamilyState(string $familyState): self
    {
        $this->familyState = $familyState;

        return $this;
    }

    public function getDrivingLicence(): ?bool
    {
        return $this->drivingLicence;
    }

    public function setDrivingLicence(bool $drivingLicence): self
    {
        $this->drivingLicence = $drivingLicence;

        return $this;
    }
}
