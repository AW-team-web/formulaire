<?php

namespace App\Entity;

use App\Repository\JobRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JobRepository::class)
 */
class Job
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobTitle;

    /**
     * @ORM\Column(type="date")
     */
    private $beginingDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $stayTime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $companyName;

    /**
     * @ORM\ManyToOne(targetEntity=Nomination::class, inversedBy="jobs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $nomination;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    public function getBeginingDate(): ?\DateTimeInterface
    {
        return $this->beginingDate;
    }

    public function setBeginingDate(\DateTimeInterface $beginingDate): self
    {
        $this->beginingDate = $beginingDate;

        return $this;
    }

    public function getStayTime(): ?string
    {
        return $this->stayTime;
    }

    public function setStayTime(string $stayTime): self
    {
        $this->stayTime = $stayTime;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getNomination(): ?Nomination
    {
        return $this->nomination;
    }

    public function setNomination(?Nomination $nomination): self
    {
        $this->nomination = $nomination;

        return $this;
    }
}
