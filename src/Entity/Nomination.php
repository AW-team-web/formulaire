<?php

namespace App\Entity;

use App\Repository\NominationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NominationRepository::class)
 */
class Nomination
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="nominations")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity=PersonnalInfos::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $personnalInfos;

    /**
     * @ORM\OneToMany(targetEntity=Diploma::class, mappedBy="nomination", orphanRemoval=true, cascade={"persist"})
     */
    private $diplomas;

    /**
     * @ORM\OneToMany(targetEntity=Skill::class, mappedBy="nomination", orphanRemoval=true, cascade={"persist"})
     */
    private $skills;

    /**
     * @ORM\OneToMany(targetEntity=Job::class, mappedBy="nomination", orphanRemoval=true, cascade={"persist"})
     */
    private $jobs;

    /**
     * @ORM\OneToOne(targetEntity=Situation::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $situation;

    /**
     * @ORM\OneToOne(targetEntity=GoalsInfo::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $goalsInfo;

    /**
     * @ORM\OneToOne(targetEntity=AwData::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $awData;

    /**
     * @ORM\Column(type="integer")
     */
    private $validationState;

    /**
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private $validator;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Lang::class, mappedBy="nomination", orphanRemoval=true, cascade={"persist"})
     */
    private $langs;

    public function __construct()
    {
        $this->diplomas = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->langs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPersonnalInfos(): ?PersonnalInfos
    {
        return $this->personnalInfos;
    }

    public function setPersonnalInfos(PersonnalInfos $personnalInfos): self
    {
        $this->personnalInfos = $personnalInfos;

        return $this;
    }

    /**
     * @return Collection|Diploma[]
     */
    public function getDiplomas(): Collection
    {
        return $this->diplomas;
    }

    public function addDiploma(Diploma $diploma): self
    {
        if (!$this->diplomas->contains($diploma)) {
            $this->diplomas[] = $diploma;
            $diploma->setNomination($this);
        }

        return $this;
    }

    public function addMultipleDiploma(Array $diplomas): self {
        foreach ($diplomas as $diploma){
            if (!$this->diplomas->contains($diploma)) {
                $this->diplomas[] = $diploma;
                $diploma->setNomination($this);
            }
        }
        return $this;
    }

    public function removeDiploma(Diploma $diploma): self
    {
        if ($this->diplomas->removeElement($diploma)) {
            // set the owning side to null (unless already changed)
            if ($diploma->getNomination() === $this) {
                $diploma->setNomination(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Skill[]
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
            $skill->setNomination($this);
        }

        return $this;
    }

    public function addMultipleSkills(Array $skills): self {
        foreach ($skills as $skill){
            if (!$this->skills->contains($skill)) {
                $this->skills[] = $skill;
                $skill->setNomination($this);
            }
        }
        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        if ($this->skills->removeElement($skill)) {
            // set the owning side to null (unless already changed)
            if ($skill->getNomination() === $this) {
                $skill->setNomination(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Job[]
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob(Job $job): self
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs[] = $job;
            $job->setNomination($this);
        }

        return $this;
    }

    public function addMultipleJob(Array $jobs): self {
        foreach ($jobs as $job){
            if (!$this->jobs->contains($job)) {
                $this->jobs[] = $job;
                $job->setNomination($this);
            }
        }
        return $this;
    }

    public function removeJob(Job $job): self
    {
        if ($this->jobs->removeElement($job)) {
            // set the owning side to null (unless already changed)
            if ($job->getNomination() === $this) {
                $job->setNomination(null);
            }
        }

        return $this;
    }

    public function getSituation(): ?Situation
    {
        return $this->situation;
    }

    public function setSituation(Situation $situation): self
    {
        $this->situation = $situation;

        return $this;
    }

    public function getGoalsInfo(): ?GoalsInfo
    {
        return $this->goalsInfo;
    }

    public function setGoalsInfo(GoalsInfo $goalsInfo): self
    {
        $this->goalsInfo = $goalsInfo;

        return $this;
    }

    public function getAwData(): ?AwData
    {
        return $this->awData;
    }

    public function setAwData(AwData $awData): self
    {
        $this->awData = $awData;

        return $this;
    }

    public function getValidationState(): ?int
    {
        return $this->validationState;
    }

    public function setValidationState(int $validationState): self
    {
        $this->validationState = $validationState;

        return $this;
    }

    public function getValidator(): ?String
    {
        return $this->validator;
    }

    public function setValidator(?String $validator): self
    {
        $this->validator = $validator;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Lang[]
     */
    public function getLangs(): Collection
    {
        return $this->langs;
    }

    public function addLang(Lang $lang): self
    {
        if (!$this->langs->contains($lang)) {
            $this->langs[] = $lang;
            $lang->setNomination($this);
        }

        return $this;
    }

    public function addMultipleLang(Array $langs): self {
        foreach ($langs as $lang){
            if (!$this->langs->contains($lang)) {
                $this->langs[] = $lang;
                $lang->setNomination($this);
            }
        }
        return $this;
    }

    public function removeLang(Lang $lang): self
    {
        if ($this->langs->removeElement($lang)) {
            // set the owning side to null (unless already changed)
            if ($lang->getNomination() === $this) {
                $lang->setNomination(null);
            }
        }

        return $this;
    }

    public function __toString(): String
    {
       return "Candidature n°".$this->getId();
    }
}
