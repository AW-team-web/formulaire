<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
    * @Id
    * @GeneratedValue
    * @Column(type="integer")
    */
    private $id;

    /**
     * @Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @Column(type="json")
     */
    private $roles = [];

    /**
     * @Column(type="string")
     */
    private $password;

    /**
    * @OneToOne(targetEntity=Adress::class, cascade={"persist", "remove"})
    * @JoinColumn(nullable=false)
    */
    private $Adress;

    /**
     * @Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity=Nomination::class, mappedBy="user")
     */
    private $nominations;

    public function __construct()
    {
        $this->nominations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = "ROLE_USER";

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getAdress(): ?Adress
    {
        return $this->Adress;
    }

    public function setAdress(Adress $Adress): self
    {
        $this->Adress = $Adress;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection|Nomination[]
     */
    public function getNominations(): Collection
    {
        return $this->nominations;
    }

    public function getNominationNumber(): int{
        return count($this->nominations);
    }

    public function addNomination(Nomination $nomination): self
    {
        if (!$this->nominations->contains($nomination)) {
            $this->nominations[] = $nomination;
            $nomination->setUser($this);
        }

        return $this;
    }

    public function removeNomination(Nomination $nomination): self
    {
        if ($this->nominations->removeElement($nomination)) {
            // set the owning side to null (unless already changed)
            if ($nomination->getUser() === $this) {
                $nomination->setUser(null);
            }
        }

        return $this;
    }

    public function __toString(): String
    {
        return $this->getFirstname()." ".$this->getLastname();
    }


}
