<?php

namespace App\Entity;

use App\Repository\GoalsInfoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GoalsInfoRepository::class)
 */
class GoalsInfo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $xpIntoDomain;

    /**
     * @ORM\Column(type="boolean")
     */
    private $expertMet;

    /**
     * @ORM\Column(type="text")
     */
    private $qualityPoints;

    /**
     * @ORM\Column(type="text")
     */
    private $professionnalProject;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeOfCompanyWanted;

    /**
     * @ORM\Column(type="text")
     */
    private $weakpoints;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getXpIntoDomain(): ?bool
    {
        return $this->xpIntoDomain;
    }

    public function setXpIntoDomain(bool $xpIntoDomain): self
    {
        $this->xpIntoDomain = $xpIntoDomain;

        return $this;
    }

    public function getExpertMet(): ?bool
    {
        return $this->expertMet;
    }

    public function setExpertMet(bool $expertMet): self
    {
        $this->expertMet = $expertMet;

        return $this;
    }

    public function getQualityPoints(): ?string
    {
        return $this->qualityPoints;
    }

    public function setQualityPoints(string $qualityPoints): self
    {
        $this->qualityPoints = $qualityPoints;

        return $this;
    }

    public function getProfessionnalProject(): ?string
    {
        return $this->professionnalProject;
    }

    public function setProfessionnalProject(string $professionnalProject): self
    {
        $this->professionnalProject = $professionnalProject;

        return $this;
    }

    public function getTypeOfCompanyWanted(): ?string
    {
        return $this->typeOfCompanyWanted;
    }

    public function setTypeOfCompanyWanted(string $typeOfCompanyWanted): self
    {
        $this->typeOfCompanyWanted = $typeOfCompanyWanted;

        return $this;
    }

    public function getWeakpoints(): ?string
    {
        return $this->weakpoints;
    }

    public function setWeakpoints(string $weakpoints): self
    {
        $this->weakpoints = $weakpoints;

        return $this;
    }
}
