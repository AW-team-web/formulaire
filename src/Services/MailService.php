<?php

namespace App\Services;

use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class MailService
{
    private MailerInterface $mailer;

    /**
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Send a contact mail
     * @param array $contactData The necessary data to send the contact mail
     * @return bool
     */
    public function sendContactMail(array $contactData):bool{
        $mail = new Email();
        $mail->from(new Address($contactData["client_mail"], $contactData["firstname"]." ".$contactData["lastname"]));
        $mail->text($contactData["mail_content"]);
        $mail->subject($contactData["mail_object"]);
        $mail->to("contact-candidat@apprentiwebeur.com");
        try {
            $this->mailer->send($mail);
            return true;
        } catch (TransportExceptionInterface $exception){
            return false;
        }
    }

    /**
     * Send a notification mail after a nomination is posted by the user
     * @param string $client_mail mail of the user
     * @param string $firstname firstname of the user
     * @param string $lastname lastname of the user
     * @param string $course_slug slug of the submitted course
     * @return bool
     */
    public function sendNominationNotificationMail(string $client_mail, string $firstname, string $lastname, string $course_slug):bool{
        $mail = new Email();
        $mail->from("no-reply@apprenti-webeur.fr");
        $mail->text("Bonjour nous vous confirmons la bonne reception de votre candidature pour la formation ".$course_slug.".");
        $mail->subject("Réception de votre candidature");
        $mail->to(new Address($client_mail, $firstname." ".$lastname));
        try {
            $this->mailer->send($mail);
            return true;
        } catch (TransportExceptionInterface $exception){
            return false;
        }
    }

    /**
     * Send a notification mail after a nomination is validated by the admin or supervisor
     * @param string $client_mail mail of the user
     * @param string $firstname firstname of the user
     * @param string $lastname lastname of the user
     * @param string $course_slug slug of the submitted course
     * @return bool
     */
    public function sendNominationConfirmMail(string $client_mail, string $firstname, string $lastname, string $course_slug):bool{
        $mail = new Email();
        $mail->from("no-reply@apprenti-webeur.fr");
        $mail->text("Bonjour nous vous avons le plaisir de vous confirmer que vous êtes admis sur la formation ".$course_slug.".");
        $mail->subject("Validation de votre candidature");
        $mail->to(new Address($client_mail, $firstname." ".$lastname));
        try {
            $this->mailer->send($mail);
            return true;
        } catch (TransportExceptionInterface $exception){
            return false;
        }
    }

    /**
     * Send a notification mail after a nomination is rejected by the admin or supervisor
     * @param string $client_mail mail of the user
     * @param string $firstname firstname of the user
     * @param string $lastname lastname of the user
     * @param string $course_slug slug of the submitted course
     * @return bool
     */
    public function sendNominationRejectMail(string $client_mail, string $firstname, string $lastname, string $course_slug):bool{
        $mail = new Email();
        $mail->from("no-reply@apprenti-webeur.fr");
        $mail->text("Bonjour nous sommes au regret de vous apprendre que votre candidature pour la formation ".$course_slug." à été rejetée.");
        $mail->subject("Rejet de votre candidature");
        $mail->to(new Address($client_mail, $firstname." ".$lastname));
        try {
            $this->mailer->send($mail);
            return true;
        } catch (TransportExceptionInterface $exception){
            return false;
        }
    }

}