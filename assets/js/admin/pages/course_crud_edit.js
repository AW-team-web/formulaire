setEditCoursePanel();
function setEditCoursePanel() {
    const checkbox = document.querySelector("#Course_page_created");
    const formGroupList = document.querySelectorAll("div.course-edit");
    if(checkbox !== null && formGroupList !== null){
        let value = checkbox.getAttribute('checked');
        let boolValue = value === 'checked';
        if (value !== 'checked'){
            formGroupList.forEach((el) => el.classList.add('d-none'));
        }
        checkbox.addEventListener("change", (e) => {
            boolValue = !boolValue;
            if (!boolValue){
                formGroupList.forEach((el) => el.classList.add('d-none'));
            } else {
                formGroupList.forEach((el) => el.classList.remove('d-none'));
            }
        });
    }
}
