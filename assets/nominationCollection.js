const addCollectionButton = document.querySelector('.add-field-button');
const removeCollectionLinks = document.querySelectorAll('.delete-field-link')

removeCollectionLinks.forEach(link => link.addEventListener('click', (e) => deleteItems(e)))
addCollectionButton.addEventListener('click', (e) => addItem(e))

function addItem(e) {
    e.preventDefault()
    const list = document.querySelector('#collection-field-list')
    let newForm = list.dataset.prototype.replace(/__name__/g, list.dataset.index)
    list.dataset.index++
    const diplomaField = document.createElement('li')
    const deleteLink = document.createElement('a')
    deleteLink.text = 'Supprimer'
    deleteLink.classList.add(...['button', 'button-outlined', 'delete-field-link', 'mb-3'])
    deleteLink.setAttribute('role', 'button')
    diplomaField.innerHTML = newForm
    diplomaField.querySelector('div').appendChild(deleteLink)
    diplomaField.classList.add('form-collection-field')
    list.appendChild(diplomaField)
    deleteLink.addEventListener('click', (e) => deleteItems(e))
}

function deleteItems(e) {
    const list = document.querySelector('#collection-field-list')
    const itemToDelete = e.target.closest('.form-collection-field')
    list.removeChild(itemToDelete)
}