/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import "@fortawesome/fontawesome-free/js/all.min.js"
import "bootstrap/dist/js/bootstrap"
import "./styles/app.scss"

// Get bootstrap icons
require('bootstrap-icons/font/bootstrap-icons.css');

const mobileMenu = document.querySelector("[data-aw-menu = content]");
const closeMenuButton = document.querySelector("[data-aw-menu = close]");
const openMenuButton = document.querySelector("[data-aw-menu = open]");
const menuOverlay = document.querySelector("[data-aw-menu = overlay]");

openMenuButton.addEventListener("click", openMenu);
closeMenuButton.addEventListener("click", closeMenu);
menuOverlay.addEventListener("click", closeMenu);

function openMenu() {
    menuOverlay.style.display = "block";
    mobileMenu.style.width = "250px";
}

function closeMenu(){
    mobileMenu.style.width = 0;
    menuOverlay.style.display = "none";
}