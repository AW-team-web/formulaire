# Technologie Front-end

- Webpack pour builder les sources
- Sass en tant que préprocesseur css
- JS Vanilla pour tous les scripts dynamiques
- Bootstrap comme framework css
- Fontawesome pour les icones

## Organisation du dossier assets

<img alt="capture structure dossier assets" src="assets/README-img/Capture-tree-assets.png" width="300"/>

### Le fichier app.js

Le fichier app.js contient les imports des librairies externes tels que fontawesome, 
il contient également les scripts js de toute l'application (generale à toutes les pages). 
On y importe également le fichier de style generale à toutes les pages. Ce fichier est 
importé dans le base template qui est commun à toutes les pages. Le fait d'importer la
feuille de style générale ici permet de la faire passer dans webpack et donc de compiler le css.
Une fois compilé nous auront dans le dossier **`/public/build`** un fichier `app.js` et un fichier `app.css`

Aperçu du fichier app.js :

![fichier app.js aperçu](README-img/app%20js%20description.png)

### Le dossier styles

Ce dossier est composé de plusieurs sous-dossier : 
 - `Components` qui correspond aux feuilles de styles de nos composants.
 - `Form_theme` qui correspond à la feuille de styles du theme général des formulaires.
 - `General_settings` qui correspond aux feuilles de styles contenant des classes ou variables s'appliquant sur toute l'application.
 - `Pages` qui correspond aux feuilles de styles contenant des règles applicables sur une seule page (Attention à la spécificité de vos sélecteurs).

### Le fichier app.scss

C'est dans ce fichier que sont importés toutes les feuilles de styles contenus dans les sous-dossier cité précédemment.
Les différents imports de ce fichier sont placés dans un ordre précis, il est donc déconseillé de modifier cet ordre.
Si vous devez rajouter des imports ne toucher qu'aux imports **Components** ou **Pages** en rajoutant les vôtre dans la bonne section.

Voici un apercu du fichier : 

![image du fichier app.scss](README-img/app-styles-description.png)

### Details du dossier General_settings

Le dossier General_settings contient plusieurs fichiers : 

- `bootstrap_overrides` c'est ici que vous devrez placer vos variables si vous voulez prendre le dessus des varibales par défault bootstrap.
**Attention ne surtout pas modifier l'ordre d'import à l'intérieur du fichier et dans app.scss**. C'est ici qu'est importé la partie
scss de bootstrap

![fichier bootstrap_overrides aperçu](README-img/bootstrap_overrides%20description.png)

- `general_settings` c'est ici que vous pourrez créer des classes que l'on retrouvera partout sur l'application.
Il est possible de les créer directement dans le fichier ou dans un autre fichier qu'on importera à la suite des imports deja present.
cf: fichier grid.scss

![fichier general_settings aperçu](README-img/general_settings%20description.png)

- `grid` c'est ici qu'est stocké la logique de notre système de grid. Sachant que le systeme de grid de bootstrap est basé sur flexbox
il était plus judicieux de refaire un système de grid basé sur css-grid. Son fonctionnement sera detaillé un peu plus bas 
dans ce README.
- `mixins` et `variables` c'est ici que sont stockés nos variables et mixins personnalisé. Il est possible d'en rajouter dans leurs fichiers respectifs.
Ces deux fichiers sont importés dans general_settings cela permet d'y avoir accès dans toute l'application.

## Détails sur nos classes et variables personnalisées
Certaines classes et variables utilisables sur l'application ne sont pas natives de bootstrap. Elles ont été développé spécifiquement pour l'application.


### Variables spacers
Les variables `$spacer-(id espacement)` permettent, lorsque vous faites des classes css personalisée,
de relier vos padding, margin, ou autre aux valeurs utilisées dans les mixins bootstrap native. 

Par exemple si je veux que mon `padding` dans mon css perso soit égale à la valeur de la classe `p-3` de bootstrap,
il me suffit d'écrire `padding: $spacer-3`. De même pour margin et autres.

### Width personalisée
Bootstrap fournie des classes tel que `w-50` pour définir la largeur d'un élément à 50%. Cependant il est impossible
de définir une largeur pour un breakpoint donnée. Grace à la classe `w-(largeur)-(breakpoint)` c'est maintenant possible.

Par exemple si je veux que mon élément prenne 50% de la largeur à partir du breakpoint sm, il me suffit de lui donner
la classe `w-50-sm`

### Systeme de grid
Bootstrap se base sur un système de grid basé lui-même sur flexbox, pas tres optimisé. Notre système de grid est basé sur
les breakpoints bootstrap natifs cependant il utilise le système css-grid. 

Par exemple si je veux une grid composé d'une colonne sur téléphone, 2 colonnes sur tablette et 4 colonnes sur desktop. 
Il me suffit de donner la classe `grid` à mon élément. Celle-ci définit l'élément en `display:grid` j'ai juste à rajouter
la classe `grid-layout-(nb de collones sur tablette)-(nb de colonnes sur desktop)` ce qui donne à la fin, 
`class="grid grid-layout-2-4"` pour le moment le nombre maximal de colonnes est de 6. Et voila un system de grid performant 
basé sur css-grid.


